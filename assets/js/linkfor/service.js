
// var api = "http://127.0.0.1:8000";

var api = "https://www.linkfor.com.br/admin/public";

var token = window.localStorage.getItem('token');
var client_id = window.localStorage.getItem('usuario.id');

/**
 * Função para realizar login do usuário na API
 */
function login(){
  var display1 = document.getElementById('loader').style.display;
  var display2 = document.getElementById('loader2').style.display;
  if(display1 == "none" && display2 == "block"){
    document.getElementById('loader').style.display = 'block';
    document.getElementById('loader2').style.display = 'none';
  }
  let recebe;
  fetch(api+'/oauth/token', {
    method: 'POST',
    headers: new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded'
    }),
    body:
      'grant_type=password'+
      '&username='+document.getElementById('email').value+
      '&password='+document.getElementById('senha').value+
      '&client_id='+4+
      '&client_secret=1FFp0htNIQw39F2hVoaXt6wUL3IbUplg1hZmcsEn'+
      '&scope='
  })
  .then((res) => {
    if(res.ok == false){
      document.getElementById('loader').style.display = 'none';
      document.getElementById('loader2').style.display = 'block';
      document.getElementById('sucesso').style.display = 'block';
    }
    else{
      if(res.ok == true){
        res.json()
        .then(body =>{
          listarUsuario(body.access_token);
        });
      }
    };

  }).catch((err) => {
    document.getElementById('loader').style.display = 'none';
    document.getElementById('loader2').style.display = 'block';
    document.getElementById('sucesso').style.display = 'block',
    console.log(err)
  })
}

/**
 * Função para listar o usuário que fez login
 * @param {*} token
 */
function listarUsuario(token){
  window.localStorage.setItem('token', token),
  fetch(api+'/api/user/visualizar-perfil', {
    method: 'GET',
    headers: new Headers({
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    }),
  })
  .then((res) => {
    console.log(res.json().then(oi => condicacao(window.localStorage.setItem('usuario',JSON.stringify(oi)))));
  })
}

/**
 * Função para atualizar o perfil do usuário
 */
function atualizarPerfil(){
  fetch(api+'/api/user/atualizar', {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    }),
    body:
      '&name='+document.getElementById('nomeCompleto').value+
      '&college='+document.getElementById('instituicaoEnsino').value+
      '&course='+document.getElementById('curso').value+
      '&dd='+document.getElementById('dd').value+
      '&telefone='+document.getElementById('celular').value+
      '&cep_endereco='+document.getElementById('cep').value+
      '&numero_banco='+document.getElementById('numero').value+
      '&mes_banco='+document.getElementById('mes').value+
      '&ano_banco='+document.getElementById('ano').value+
      '&cpf_cnpj='+document.getElementById('cpf').value+
      '&endereco_endereco='+document.getElementById('endereco').value+
      '&sobre_voce='+document.getElementById('sobreVoce').value+
      '&especialidades='+document.getElementById('especialidades').value+
      '&formacao='+document.getElementById('formacao').value+
      '&experiencia_profissional='+document.getElementById('experiencia').value+
      '&cursos='+document.getElementById('cursos').value+
      '&email='+document.getElementById('email').value
  })
  .then((res) => {
    res.json().then(body =>
      window.localStorage.setItem('usuario', JSON.stringify(body)),
      window.location.href = 'project.html',
      alert('Conta atualizada com sucesso!')
    );
  }).catch((err) => {
    console.log(err);
  })
}

/**
 * Função para mostrar a área do usuário especifica de acordo com seu perfil
 * @param {*} dados
 */
function condicacao(dados){
  if(JSON.parse(window.localStorage.getItem('usuario')).situacao == "bloqueado" && JSON.parse(window.localStorage.getItem('usuario')).perfil == "profissional"){
    window.location.href = 'area/erro/erro.html';
  }
  else{
    if(JSON.parse(window.localStorage.getItem('usuario')).situacao == "ativo" && JSON.parse(window.localStorage.getItem('usuario')).perfil == "profissional"){
      console.log('Ativado');
      window.location.href = 'area/project.html';
    }
    else{
      if(JSON.parse(window.localStorage.getItem('usuario')).situacao == "ativo" && JSON.parse(window.localStorage.getItem('usuario')).perfil == "cliente"){
        window.location.href = 'area/clients.html';
      }
      else{
        if(JSON.parse(window.localStorage.getItem('usuario')).situacao == "ativo" && JSON.parse(window.localStorage.getItem('usuario')).perfil == "admin"){
          window.location.href = 'area/admin-blqueio.html';
        }
      }
    }
  }
}

/**
 * Função para criar conta de cliente
 */
function criarContaCliente(){
  var display1 = document.getElementById('loader').style.display;
  var display2 = document.getElementById('loader2').style.display;
  if(display1 == "none" && display2 == "block"){
    document.getElementById('loader').style.display = 'block';
    document.getElementById('loader2').style.display = 'none';
  }

  fetch(api+'/api/cadastro-cliente', {
    method: 'POST',
    headers: new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded'
    }),
    body:
      '&name='+document.getElementById('nome_cliente').value+
      '&email='+document.getElementById('email_cliente').value+
      '&password='+document.getElementById('senha').value+
      '&situacao=ativo'+
      '&dd='+document.getElementById('dd').value+
      '&telefone='+document.getElementById('telefone').value
  })
  .then((res) => {

    res.json()
    .then(body =>{
      if(body.errors == undefined){
        alert('Conta criada com sucesso! Suas informações estão sendo avaliadas e no prazo máximo de 1 semana entraremos em contato por e-mail.');
        window.location.href = 'login.html';
      }
      else{
        document.getElementById('email_uso').style.display = 'block';
        setTimeout(() => {
          document.getElementById('email_uso').style.display = 'none';
        }, 20000)
        alert('O valor informado para o campo e-mail já está em uso.');
        console.log('O valor informado para o campo e-mail já está em uso.');
      }
      document.getElementById('loader').style.display = 'none';
      document.getElementById('loader2').style.display = 'block';
    })
    .catch(erro => {
      console.log('OITRO: ', erro);
    })
  }).catch((err) => {
    document.getElementById('loader').style.display = 'none';
    document.getElementById('loader2').style.display = 'block';
    // document.getElementById('sucesso').style.display = 'block',
    console.log(err)
  })
}

/**
 * Função para criar conta de profissional
 */
function criarContaProfissional(){
  console.log("Oi");
  var vetor = [];
  if(document.getElementById("desenvolvedor_web").checked == true){
    vetor.push('desenvolvedor_web');
    console.log('Web');
  }
  if(document.getElementById("desenvolvedor_mobile").checked == true){
    vetor.push('desenvolvedor_mobile');
    console.log('Mobile');
  }
  if(document.getElementById("design").checked == true){
    vetor.push('designer');
    console.log('Design');
  }

  fetch(api+'/api/cadastro-profissional', {
    method: 'POST',
    headers: new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded'
    }),
    body:
      '&name='+document.getElementById('nome').value+
      '&email='+document.getElementById('email').value+
      '&password='+document.getElementById('senha').value+
      '&dd='+document.getElementById('dd').value+
      '&telefone='+document.getElementById('telefone').value+
      '&situacao=bloqueado'+
      '&cidade='+$("#cidade").val()+
      '&estado='+$("#estado").val()+
      '&pais=Brasil'+

      '&formacao='+document.getElementById('formacao').value+
      '&college='+document.getElementById('universidade').value+
      '&course='+document.getElementById('curso').value+
      '&area='+vetor+

      '&descricao='+document.getElementById('descricao').value+
      '&melhor_habilidade='+document.getElementById('melhor_habilidade').value+
      '&link_linkedin='+document.getElementById('linkedin').value+
      '&link_github='+document.getElementById('github').value+
      '&link_portfolio='+document.getElementById('link').value+

      '&tempo_experiencia='+document.getElementById('tempo_experiencia').value+
      '&melhor_projeto='+document.getElementById('melhor_projeto').value+
      '&link_melhor_projeto='+document.getElementById('link_projeto').value+
      '&experiencia_remoto='+document.getElementById('customCheck1').value+
      '&organizacao_tempo='+document.getElementById('organizacao_tempo').value+
      '&ambiente_ideal='+document.getElementById('ambiente_ideal').value
  })
  .then((res) => {
    res.json()
    .then(body =>{
      if(body.errors == undefined){
        alert('Conta criada com sucesso! Suas informações estão sendo avaliadas e no prazo máximo de 1 semana entraremos em contato por e-mail.');
        window.location.href = 'login.html';
      }
      else{
        document.getElementById('email_uso').style.display = 'block';
        setTimeout(() => {
          document.getElementById('email_uso').style.display = 'none';
        }, 20000)
        alert('O valor informado para o campo e-mail já está em uso.');
        console.log('O valor informado para o campo e-mail já está em uso.');
      }
    })
    .catch(erro => {
      console.log('Erro: ', erro);
    })
  }).catch((err) => {
    console.log(err);
  })
}

/**
 * Função para realizar a busca de profissionais de acordo a área
 */
function pesquisar(){
  $('#checkboxes').hide()
  window.localStorage.removeItem('area');
  console.log('DAD: ', document.getElementById("profissional").value)
  // let area = [];
  // if(document.getElementById("desenvolvedor_web").checked == true){
  //   area.push('desenvolvedor_web');
  //   console.log('Desenvolvedor Web');
  // }
  // if(document.getElementById("desenvolvedor_mobile").checked == true){
  //   area.push('desenvolvedor_mobile');
  //   console.log('Desenvolvedor Mobile');

  // }
  // if(document.getElementById("designer").checked == true){
  //   area.push('designer');
  //   console.log('Designer');
  // }

  fetch(api+'/api/user/buscar-area/'+document.getElementById("profissional").value, {
    method: 'POST',
    headers: new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded'
    }),
  })
  .then((res) => {
    res.json()
    .then(usuarios => {
      console.log('USUARIOS: ', usuarios.data);
      document.getElementById('loader').style.display = 'block';
      document.getElementById('buscar_user').style.display = 'none';
      window.localStorage.setItem('area', JSON.stringify(usuarios.data));
      window.location.href = 'busca.html';
    });

    // res.json().then(body =>
      // console.log('COOD: ', body.data),
      // console.log('CORPO'),
      // window.localStorage.setItem('area', JSON.stringify(body.data)),
      // window.location.href = 'busca.html',
      // console.log('CORPO 2'),
    // );
  }).catch((err) => {
    console.log(err)
  });
}

function solicitarServico(){
 //Falta pegar o id
  fetch(api+'/api/solicitacao/solicitar/'+id,{
    method: 'POST',
    headers: new Headers({
      'Accept': 'application/json',
      'Content-Type': 'application/x-www-form-urlencoded'
    }),
    body:
    '&cliente_id='+document.getElementById('idCliente').value+
    '&observacao='+document.getElementById('observacao').value+
    '&nome_projeto='+document.getElementById('nomeProjeto').value+
    '&tipo_servico='+document.getElementById('tipoServico').value+
    '&descricao_projeto='+document.getElementById('descricaoProjeto').value
  })
  .then((rest)=>{
    rest.json().then(body =>
      alert("Solicitação enviada. Assim que o profissional analisar entrará em contato com você!")
    )
  }).catch((err)=>{
    console.log(err)
  })
}

function contato(){
  var display1 = document.getElementById('loader').style.display;
  var display2 = document.getElementById('loader2').style.display;
  if(display1 == "none" && display2 == "block"){
    document.getElementById('loader').style.display = 'block';
    document.getElementById('loader2').style.display = 'none';
  }

  fetch(api+'/api/contato/enviar', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }),
    body:
      '&name='+document.getElementById('nome').value+
      '&email='+document.getElementById('email').value+
      '&mensagem='+document.getElementById('mensagem').value
  })
  .then((res) => {
    res.json().then(body =>

      document.getElementById('loader').style.display = 'none',
      document.getElementById('loader2').style.display = 'block',
      document.getElementById('sucesso').style.display = 'block',

      document.getElementById('nome').value='',
      document.getElementById('email').value='',
      document.getElementById('mensagem').value=''
    ),(erro => {
      console.log('Dey erriti 1: ', erro);
    });
  }).catch((err) => {
    document.getElementById('loader').style.display = 'none';
    document.getElementById('loader2').style.display = 'block';
  })
}

function anunciarVaga(){
  var display1 = document.getElementById('loader').style.display;
  var display2 = document.getElementById('loader2').style.display;
  if(display1 == "none" && display2 == "block"){
    document.getElementById('loader').style.display = 'block';
    document.getElementById('loader2').style.display = 'none';
  }
  fetch(api+'/api/recrutar/criar', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }),
    body:
      '&nome=' + document.getElementById('nome').value+
      '&telefone=' + document.getElementById('telefone').value+
      '&email=' + document.getElementById('email').value+
      '&empresa=' + document.getElementById('empresa').value+
      '&tipo_profissional=' + document.getElementById('tipo_profissional').value+
      '&requisitos_vaga=' + document.getElementById('descricao_vaga').value+
      '&salario=' + document.getElementById('salario').value+
      '&diferenciais=' + document.getElementById('diferenciais').value+
      '&complemento=' + document.getElementById('complemento').value
  })
  .then((res) => {
    res.json().then(body =>{
      document.getElementById('loader').style.display = 'none';
      document.getElementById('loader2').style.display = 'block';
      document.getElementById('sucesso').style.display = 'block';

      document.getElementById('nome').value='';
      document.getElementById('telefone').value='';
      document.getElementById('email').value='';
      document.getElementById('empresa').value='';
      document.getElementById('tipo_profissional').value='';
      document.getElementById('descricao_vaga').value='';
      document.getElementById('salario').value='';
      document.getElementById('diferenciais').value='';
      document.getElementById('complemento').value='';
    })
    .catch(erro => {
      console.log('ERRO 1: ', erro);
    })

  }).catch((err) => {
    console.log('ERRO: ', err)
    document.getElementById('loader').style.display = 'none';
    document.getElementById('loader2').style.display = 'block';
  })
}
/**
 * Função para carregar os estados de acordo com o IBGE
 */
function preencherEstadosCidades(){
  $('#estado').on("change", function(){
    var id = $(this).val();
    ajaxCall(id);
  });
}

/**
 * Função para preencher as cidades
 */
function preencherTocantins(){
  $.ajax({
    type: "GET",
    url: "https://servicodados.ibge.gov.br/api/v1/localidades/estados",
    success: function(data){
      estado = $('#estado');
      estado.empty();
      estado.append('<option value=' + 0 + '>' + 'Selecione o estado' + '</option>');
      for(i=0; i < data.length; i++){
        estado.append("<option value='" + data[i].id + "'>" + data[i].nome + "</option>");
      }
      ajaxCall(17);
    }
  });
}

/**
 * Função para carregar id do estado para mostrar a cidade
 * @param {*} id
 */
function ajaxCall(id){
  $.ajax({
    type: "GET",
    url: "https://servicodados.ibge.gov.br/api/v1/localidades/estados/"+id+"/municipios",
    success: function( data ) {
      var selectM = $('#cidade');
      selectM.empty();
      selectM.append('<option value=' + 0 + '>' + 'Selecione a cidade' + '</option>');
      for(i=0; i < data.length; i++) {
        selectM.append('<option value=' + data[i].id + '>' + data[i].nome + '</option>');
      }
      console.log("Municípios - Sucesso");
    },
    error: function(result) {
        alert("Data not found");
    }
  });
}


function dadosPessoais(){
  let dados = {
    name: document.getElementById('nome').value,
    email: document.getElementById('email').value,
    password: document.getElementById('senha').value,
    dd: document.getElementById('dd').value,
    telefone: document.getElementById('telefone').value,
    cidade: $("#cidade").val(),
    estado: $("#estado").val(),
  };

  window.localStorage.setItem('cadastro', JSON.stringify(dados));
  console.log('DAOS: ', dados);

  $('.nav-tabs a').click(function(){
    $(this).tab('show');
  })
}

function solicitarContratoProfissional(){
  fetch(api+'/api/contrato/criar', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }),
    body:
      '&nome_profissional=' + document.getElementById('nome_profissional').value+
      '&cpf_profissional=' + document.getElementById('cpf_profissional').value+
      '&rg_profissional=' + document.getElementById('rg_profissional').value+
      '&cnpj_profissional=' + document.getElementById('cnpj_profissional').value+
      '&cep=' + document.getElementById('cep_profissional').value+
      '&endereco=' + document.getElementById('cep_profissional').value+
      '&numero=' + document.getElementById('cep_profissional').value+
      '&estado=' + document.getElementById('estado').value+
      '&cidade=' + document.getElementById('cidade').value+
      '&bairro=' + document.getElementById('complemento_profissional').value+
      '&valor_projeto=' + document.getElementById('valor_projeto').value+
      '&escopo_projeto=' + document.getElementById('escopo_projeto').value+
      '&prazo_execucao=' + document.getElementById('prazo_projeto').value+
      '&id_solicitacao=' + window.localStorage.getItem('id_projeto_contrato')
  })
  .then((res) => {
    res.json().then(cliente =>{
      console.log('CLIENTE: ', cliente)
    })
    .catch(erro => {
      console.log('ERRO 1: ', erro);
    })

  }).catch((err) => {
    console.log('ERRO: ', err);
  })
}

function solicitarContratoCliente(){
  fetch(api+'/api/recrutar/criar', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json'
    }),
    body:
      '&nome_cliente=' + document.getElementById('nome_cliente').value+
      '&cpf_cliente=' + document.getElementById('cpf_cliente').value+
      '&rg_cliente=' + document.getElementById('rg_cliente').value+
      '&cnpj_cliente=' + document.getElementById('cnpj_cliente').value+
      '&cep_cliente=' + document.getElementById('cep_cliente').value+
      '&endereco_cliente=' + document.getElementById('endereco_cliente').value+
      '&numero_cliente=' + document.getElementById('numero_cliente').value+
      '&bairro=' + document.getElementById('complemento_cliente').value
  })
  .then((res) => {
    res.json().then(cliente =>{
      console.log('CLIENTE: ', cliente)
    })
    .catch(erro => {
      console.log('ERRO 1: ', erro);
    })

  }).catch((err) => {
    console.log('ERRO: ', err);
  })
}