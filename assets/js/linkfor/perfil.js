dadosUser = JSON.parse(window.localStorage.getItem('perfil'));

document.getElementById('nome').innerHTML = dadosUser.name;
document.getElementById('area').innerHTML = dadosUser.area;
document.getElementById('curso').innerHTML = dadosUser.course;
document.getElementById('universidade').innerHTML = dadosUser.college;
document.getElementById('descricao').innerHTML = dadosUser.descricao;
document.getElementById('portfolio').innerHTML = dadosUser.link_portfolio;
document.getElementById('sobre_voce').innerHTML = dadosUser.sobre_voce;
document.getElementById('especialidades').innerHTML = dadosUser.especialidades;
document.getElementById('formacao').innerHTML = dadosUser.formacao;
document.getElementById('experiencia_profissional').innerHTML = dadosUser.experiencia_profissional;
document.getElementById('cursos').innerHTML = dadosUser.cursos;

