var area = JSON.parse(window.localStorage.getItem('area'));
let dados, dados2;
var buscarProf = document.getElementById('resultado');
var dadosProfissional = [];
var perfilProfissional = [];

var contador = 0, contMostra = 0;
area.forEach(element => { 
    if(element.situacao != 'bloqueado'){
        console.log('CONTADOR: ', contador);
        if(contador <= 3){
            var div = document.createElement('div');
            div.setAttribute("class", "col-md-12 col-lg-6 col-sm-12 col-xl-3");
            div.innerHTML = buscar(element);
            buscarProf.appendChild(div);
        }
        contador++;
        contMostra++;
    }
});
if(contMostra == 0){
    var div = document.createElement('div');
    div.setAttribute("class", "col-md-12 col-lg-12 col-sm-12 col-xl-12");
    div.innerHTML = buscarVazio();
    buscarProf.appendChild(div);
}
// document.getElementById('quantidade').innerHTML = contador;
// contador = 0;
// if(contador == 1){
//     document.getElementById('area').innerHTML = 'Profissional';
// }
// else{
//     document.getElementById('area').innerHTML = 'Profissionais';
// }

function buscarVazio(){
    return `
        <div class="card mb-3 shadow-sm" style="border-radius: 10px;">
            <img class="card-img-top" style="display: block;margin-left: auto;margin-right: auto; overflow: hidden;width: 70px;height: 70px; margin-top: 5%;" src="assets/img/erro.svg">
            <div class="card-body">
                <h6  style="font-size: 1.5em; font-weight: bolder; color: #426941; text-align: center;">Usuários não encontrados</h6>
                <p style="font-size: 1.3em; font-weight: bolder; color: rgb(168, 168, 168); text-align: center;">Não encontramos usuários da área pesquisada, agradecemos sua busca.</p>                
            </div>
        </div>
    `
}

function buscar(element){
    console.log(element);
    if(element.area == 'desenvolvedor_web') {
        dados = 'Desenvolvedor Web';
    }
    if(element.area == 'desenvolvedor_mobile') {
        dados = 'Desenvolvedor Mobile';
    }
    if(element.area == 'designer') {
        dados = 'Designer';
    }
    if(element.area == 'desenvolvedor_web,desenvolvedor_mobile') {
        dados = 'Desenvolvedor Web e Desenvolvedor Mobile';
    }
    if(element.area == 'desenvolvedor_web,desenvolvedor_mobile,designer') {
        dados = 'Desenvolvedor Web, Desenvolvedor Mobile e Designer';
    }
    
    dadosProfissional.push(element.id);
    dadosProfissional.push(element.name);
    dadosProfissional.push(dados);
    perfilProfissional.push(element);

    return `
        <div class="card mb-3 shadow-sm" style="border-radius: 10px;">
            <img class="card-img-top" style="display: block;margin-left: auto;margin-right: auto; border-radius: 50%;overflow: hidden;width: 70px;height: 70px; margin-top: 5%;" src="assets/img/user.svg">
            <div class="card-body">
                <h6  style="font-size: 1em; font-weight: bolder; color: #426941; text-align: center;">${ element.name }</h6>
                <p style="font-size: 1em; font-weight: bolder; color: rgb(168, 168, 168); text-align: center;">${ dados }</p>
                
                <div class="d-flex justify-content-between align-items-center" style="margin-top: 20%;">
                    <div class="btn-group" style="margin: 0 auto;">
                        <button type="button" class="btn btn-sm btn-outline-secondary" style="background-color: black;">
                            <strong onclick="visualPerfil(${ element.id })">PERFIL</strong>
                        </button>
                        <br>
                        <button type="button" class="btn btn-sm btn-outline-secondary" style="background-color: #426941; color: white;">
                            <strong onclick="chamar(${ element.id })">NEGOCIAR</strong>
                        </button>
                    </div>
                </div>                
        </div>
    `
}

function chamar(id){
    // console.log('DADD: ', dadosProfissional);
    // if(id == dadosProfissional[0]){
        window.localStorage.setItem('solicitacao', id);
        window.location.href = 'servicos.html';
    // }
    // else{
    //     console.log(id)
    //     alert('Problema no processamento dos dados. Por favor, tente novamente!');
    // }
}

function visualPerfil(id){
    perfilProfissional.forEach(element => {
        if(element.id == id){
            window.localStorage.setItem('perfil', JSON.stringify(element));
            window.location.href = 'perfil.html';
        }
    });
}