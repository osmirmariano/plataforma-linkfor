// var api = "http://127.0.0.1:8000";
var api = "https://www.linkfor.com.br/admin/public";

var dadosUser;

if(window.localStorage.getItem('token') == null){
    window.location.href = 'login.html';
}
else{
    listaPerfilUsuario();
}

function listaPerfilUsuario(){
    fetch(api+'/api/user/perfil/'+window.localStorage.getItem('solicitacao'), {
        method: 'GET',
        headers: new Headers({
            'Accept': 'application/json',
        }),
    })
    .then((res) => {
        res.json().then(certo =>{
            document.getElementById('nomeUsuario').innerHTML = certo.name;
            document.getElementById('areaUsuario').innerHTML = certo.area;
        })
    });
}

function solicitarServico(){
    if(JSON.parse(window.localStorage.getItem('usuario')).perfil == "profissional"){
        alert("Seu perfil é de profissional, para realizar solicitação você deve ter conta de cliente");
    }
    else{
        let termo;
        if(document.getElementById('termo').value == 'on'){
            termo = 1;
        }
        
        fetch(api+'/api/solicitacao/solicitar/' + JSON.parse(window.localStorage.getItem('solicitacao')), {
          method: 'POST',
          headers: new Headers({
            'Content-Type': 'application/x-www-form-urlencoded',
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
          }),
          body:
            '&observacao='+document.getElementById('observacao-projeto').value+
            '&nome_projeto='+document.getElementById('nome-projeto').value+
            '&descricao_projeto='+document.getElementById('descricao-projeto').value+
            '&tipo_servico='+document.getElementById('tipo-projeto').value+
            '&cliente_id='+ JSON.parse(window.localStorage.getItem('usuario')).id+
            '&nome_cliente='+ JSON.parse(window.localStorage.getItem('usuario')).name+
            '&termo_aceite='+termo    
        })
        .then((res) => {
          res.json().then(body =>
            window.location.href = 'area/project.html',
          )
        })
        .catch((err) => {
            console.log('ERRO: ', err);
            alert('Não foi possível realizar a solicitação do serviço. Por favor, tente novamente, caso o problema persistir, notifique nós em fale conosco ' + err);
        })
    }
  }
  