let nome = JSON.parse(window.localStorage.getItem('usuario')).name.split(' ');
document.getElementById('name-user').innerHTML = nome[0] + ' ' + nome[1];

function apagarToken(){
    window.localStorage.removeItem('token');
}

// Mantendo formulário preenchido (temoporario)

document.getElementById('nomeCompleto').value = JSON.parse(window.localStorage.getItem('usuario')).name;
document.getElementById('nomeCompleto').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).name;
document.getElementById('email').value = JSON.parse(window.localStorage.getItem('usuario')).email;
document.getElementById('email').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).email;
document.getElementById('instituicaoEnsino').value = JSON.parse(window.localStorage.getItem('usuario')).college;
document.getElementById('instituicaoEnsino').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).college;
document.getElementById('curso').value = JSON.parse(window.localStorage.getItem('usuario')).course;
document.getElementById('curso').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).course;
document.getElementById('dd').value = JSON.parse(window.localStorage.getItem('usuario')).dd;
document.getElementById('celular').value = JSON.parse(window.localStorage.getItem('usuario')).telefone;
document.getElementById('cep').value = JSON.parse(window.localStorage.getItem('usuario')).cep_endereco;
document.getElementById('cidade').value = JSON.parse(window.localStorage.getItem('usuario')).city;
document.getElementById('estado').value = JSON.parse(window.localStorage.getItem('usuario')).state;
document.getElementById('endereco').value = JSON.parse(window.localStorage.getItem('usuario')).endereco_endereco;

document.getElementById('numero').value = JSON.parse(window.localStorage.getItem('usuario')).numero_banco;
document.getElementById('mes').value = JSON.parse(window.localStorage.getItem('usuario')).mes_banco;
document.getElementById('ano').value = JSON.parse(window.localStorage.getItem('usuario')).ano_banco;
document.getElementById('sobreVoce').value = JSON.parse(window.localStorage.getItem('usuario')).sobre_voce;
document.getElementById('sobreVoce').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).sobre_voce;
document.getElementById('especialidades').value = JSON.parse(window.localStorage.getItem('usuario')).especialidades;
document.getElementById('especialidades').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).especialidades;
document.getElementById('formacao').value = JSON.parse(window.localStorage.getItem('usuario')).formacao;
document.getElementById('formacao').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).formacao;
document.getElementById('experiencia').value = JSON.parse(window.localStorage.getItem('usuario')).experiencia_profissional;
document.getElementById('experiencia').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).experiencia_profissional;
document.getElementById('cursos').value = JSON.parse(window.localStorage.getItem('usuario')).cursos;
document.getElementById('cursos').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).cursos;
document.getElementById('usuarioNome').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).name;
document.getElementById('perfilUsuario').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).perfil;
document.getElementById('descricaoUsuario').innerHTML =  JSON.parse(window.localStorage.getItem('usuario')).sobre_voce;