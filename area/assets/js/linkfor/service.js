// var api = "http://127.0.0.1:8000";
var api = "https://www.linkfor.com.br/admin/public";
function listarUsuario(){
  fetch(api+'/api/user/visualizar-perfil', {
    method: 'GET',
    headers: new Headers({
      'Accept': 'application/json',
      'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    }),
  })
  .then((res) => {
    res.json().then(body => 
      window.localStorage.setItem('usuario',JSON.stringify(body)),
    );
    
    let nome = JSON.parse(window.localStorage.getItem('usuario')).name.split(' ');
    document.getElementById('name-user').innerHTML = nome[0] + ' ' + nome[1];
    
  })
  .catch((err) => {
    console.log(err)
  })
}

// if(JSON.parse(window.localStorage.getItem('usuario')).perfil == 'profissional'){
//   document.getElementById('profissional').style.display = 'block';
//   document.getElementById('cliente').style.display = 'none';
// }
// else{
//   document.getElementById('profissional').style.display = 'none';
//   document.getElementById('cliente').style.display = 'block';
// }

visualizarContrato();
listarUsuario();

function visualizarContrato(){
  fetch(api+'/api/contrato/mostrar-por-solicitacao/'+window.localStorage.getItem('id_projeto_contrato'), {
    method: 'GET',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization': 'Bearer '+window.localStorage.getItem('token')
    }),
  })
  .then((res) => {
    res.json()
    .then(mostra =>{
      console.log('Dados: ', mostra);
      if(mostra.length == 0){
        if(JSON.parse(window.localStorage.getItem('usuario')).perfil == 'profissional'){
          document.getElementById('profissional').style.display = 'block';
          document.getElementById('cliente').style.display = 'none';
          document.getElementById('mensagemFormulario').style.display = 'block';
          document.getElementById('mensagemPreencheu').style.display = 'none';
        }
        else{
          document.getElementById('profissional').style.display = 'none';
          document.getElementById('cliente').style.display = 'block';
          document.getElementById('mensagemFormulario').style.display = 'block';
          document.getElementById('mensagemPreencheu').style.display = 'none';
        }
      }
      else{
        window.localStorage.setItem('id_contrato', mostra[0].id);
        if(JSON.parse(window.localStorage.getItem('usuario')).perfil == "profissional"){
          if(mostra[0].nome_profissional == null){
            document.getElementById('profissional').style.display = 'block';
            document.getElementById('cliente').style.display = 'none';
          }
          else{
            document.getElementById('mensagemFormulario').style.display = 'none';
            document.getElementById('mensagemPreencheu').style.display = 'block';
          }
        }
        else{
          if(mostra[0].nome_cliente == null){
            console.log('AQIID')
            document.getElementById('profissional').style.display = 'none';
              document.getElementById('cliente').style.display = 'block';
              document.getElementById('mensagemFormulario').style.display = 'block';
          }
          else{
            document.getElementById('mensagemFormulario').style.display = 'none';
            document.getElementById('mensagemPreencheu').style.display = 'block';
          }
        }
      }
    })
    .catch(erro => {
      console.log('ERRO 1: ', erro);
    })

  }).catch((err) => {
    console.log('ERRO: ', err);
  });
}

function solicitarContratoProfissional(){
  fetch(api+'/api/contrato/criar', {
    method: 'POST',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization': 'Bearer '+window.localStorage.getItem('token')
    }),
    body:
      '&nome_profissional=' + document.getElementById('nome_profissional').value+
      '&cpf_profissional=' + document.getElementById('cpf_profissional').value+
      '&rg_profissional=' + document.getElementById('rg_profissional').value+
      '&cnpj_profissional=' + document.getElementById('cnpj_profissional').value+
      '&cep=' + document.getElementById('cep_profissional').value+
      '&endereco=' + document.getElementById('endereco_profissional').value+
      '&numero=' + document.getElementById('num_profissional').value+
      '&estado=' + document.getElementById('estado').value+
      '&cidade=' + document.getElementById('cidade').value+
      '&bairro=' + document.getElementById('complemento_profissional').value+
      '&valor_projeto=' + document.getElementById('valor_projeto').value+
      '&escopo_projeto=' + document.getElementById('escopo_projeto').value+
      '&prazo_execucao=' + document.getElementById('prazo_projeto').value+
      '&id_solicitacao=' + window.localStorage.getItem('id_projeto_contrato')
  })
  .then((res) => {
    res.json().then(cliente =>{
      if(cliente.erro == "Você já preencheu os dados."){
        alert('Você já preencheu os campos para gerar o contrato e nossa equipe já está providenciando');
      }
      else{
        notificacaoSucesso('top', 'right');
        window.location.href = 'contrato.html';
      }
      console.log('CLIENTE: ', cliente)
    })
    .catch(erro => {
      console.log('ERRO 1: ', erro);
    })

  }).catch((err) => {
    console.log('ERRO: ', err);
  })
}

function solicitarContratoCliente(){
  fetch(api+'/api/contrato/alterar/'+ window.localStorage.getItem('id_contrato'), {
    method: 'PUT',
    headers: new Headers({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization': 'Bearer '+window.localStorage.getItem('token')
    }),
    body:
      '&cnpj_cliente=' + document.getElementById('cnpj_cliente').value+
      '&cpf_cliente=' + document.getElementById('cpf_cliente').value+
      '&nome_cliente=' + document.getElementById('nome_cliente').value+
      '&rg_cliente=' + document.getElementById('rg_cliente').value+
      '&cep=' + document.getElementById('cep_cliente').value+
      '&endereco=' + document.getElementById('endereco_cliente').value+
      '&numero=' + document.getElementById('numero_cliente').value+
      '&bairro=' + document.getElementById('complemento_cliente').value+
      '&quantidade_parcelas=' + document.getElementById('quant_parcelas').value
      // '&estado=' + document.getElementById('estado').value+
      // '&cidade=' + document.getElementById('cidade').value+
  })
  .then((res) => {
    res.json().then(cliente =>{
      if(cliente.erro == "Você já preencheu os dados."){
        alert('Você já preencheu os campos para gerar o contrato e nossa equipe já está providenciando');
      }
      else{
        window.location.href = 'contrato.html';
        notificacaoSucesso('top', 'right');
      }
      console.log('CLIENTE: ', cliente)
    })
    .catch(erro => {
      console.log('ERRO 1: ', erro);
      notificacaoSucesso('top', 'right');
    })

  }).catch((err) => {
    console.log('ERRO: ', err);
    notificacaoSucesso('top', 'right');
  })
}

function notificacaoSucesso(from, align) {
  type = ['', 'info', 'danger','success', 'warning', 'rose', 'primary'];

  color = Math.floor((Math.random() * 10) + 1);

  $.notify({
      icon: "notifications",
      message: "Seus dados foram enviados com sucesso. Aguarde, pois nossa equipe está providenciando o contrato."

  }, {
      type: type[color],
      timer: 3000,
      placement: {
          from: from,
          align: align
      }
  });
}

function notificacaoErro(from, align) {
  type = ['', 'info', 'danger','success', 'warning', 'rose', 'primary'];

  color = Math.floor((Math.random() * 10) + 1);

  $.notify({
      icon: "notifications",
      message: "Problema com nosso servidor. Por favor, tente novamente mais tarde."

  }, {
      type: type[color],
      timer: 3000,
      placement: {
          from: from,
          align: align
      }
  });
}