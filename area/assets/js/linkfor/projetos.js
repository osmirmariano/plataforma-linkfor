// var api = "http://127.0.0.1:8000";
var api = "https://www.linkfor.com.br/admin/public";
var profClien, notaAvaliacao = 0;
if(JSON.parse(window.localStorage.getItem('usuario')).perfil == 'profissional'){
    document.getElementById('cliente-mostra').style.display = 'none';
}
else{
    document.getElementById('cliente-mostra').style.display = 'block';
}
JSON.parse(window.localStorage.getItem('usuario')).id;
if(JSON.parse(window.localStorage.getItem('usuario')).perfil == 'profissional'){
    profClien = '/api/solicitacao/projetos-profissional/';
}
else{
    profClien = '/api/solicitacao/projetos-cliente/'
}
fetch(api+profClien+JSON.parse(window.localStorage.getItem('usuario')).id, {
    method: 'GET',
    headers: new Headers({
    'Accept': 'application/json',
    'Authorization': 'Bearer '+window.localStorage.getItem('token')
    }),
})
.then(resultado => {
    resultado.json()
    .then(projetos => {
        window.localStorage.setItem('projetos', JSON.stringify(projetos.data));
        mostrarProjetos();
    })
})
.catch((err) => {
    console.log(err)
})

function mostrarProjetos(){
    var projeto = JSON.parse(window.localStorage.getItem('projetos'));
    var todosProjetosSolicitacao = document.getElementById('projetosSolicitacao');
    var todosProjetosAndamento = document.getElementById('projetosAndamento');
    var todosProjetosFinalizado = document.getElementById('projetosFinalizada');
    var todosProjetosCancelado = document.getElementById('projetosCancelada');
    
    var projetosVazioSolicitacao1 = document.getElementById('projetosVazioSolicitacao');
    var projetosVazioAndamento1 = document.getElementById('projetosVazioAndamento');
    var projetosVazioFinalizado1 = document.getElementById('projetosVazioFinalizado');
    var projetosVazioCancelado1 = document.getElementById('projetosVazioCancelado');

    let solicitacaoCont = 0, atendimentoCont = 0, finalizadoCont = 0, canceladoCont = 0;
    projeto.forEach(element => {
        if(projetos(element) != undefined){
            if(element.tipo == "solicitacao" && element.status != "cancelado"){
                var div = document.createElement('div');
                div.setAttribute("class", "col-md-12");
                div.innerHTML = projetos(element);
                todosProjetosSolicitacao.appendChild(div);
                solicitacaoCont = 1;
            }
            else if(element.tipo == "atendimento" && element.status != "cancelado"){
                var div = document.createElement('div');
                div.setAttribute("class", "col-md-12");
                div.innerHTML = projetos(element);
                todosProjetosAndamento.appendChild(div);
                atendimentoCont = 1
            }
            else if(element.tipo == "finalizado" && element.status == "pago"){
                var div = document.createElement('div');
                div.setAttribute("class", "col-md-12");
                div.innerHTML = projetos(element);
                todosProjetosFinalizado.appendChild(div);
                finalizadoCont = 1;
            }
            else{
                var div = document.createElement('div');
                div.setAttribute("class", "col-md-12");
                div.innerHTML = projetos(element);
                todosProjetosCancelado.appendChild(div);
                canceladoCont = 1;
            }
        }
    });
    
    if(solicitacaoCont == 0){
        var div = document.createElement('div');
        div.setAttribute("class", "col-md-12");
        div.innerHTML = `
            <div class="card" >
                <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                    <h4 class="title">
                        Você não possui projetos em solicitações.
                    </h4>
                    <br/>
                </div>
            </div>
        `;
        projetosVazioSolicitacao1.appendChild(div);
    }
    if(atendimentoCont == 0){
        var div = document.createElement('div');
        div.setAttribute("class", "col-md-12");
        div.innerHTML = `
            <div class="card" >
                <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                    <h4 class="title">
                        Você não possui projetos em andamento.
                    </h4>
                    <br/>
                </div>
            </div>
        `;
        projetosVazioAndamento1.appendChild(div);
    }
    if(finalizadoCont == 0){
        var div = document.createElement('div');
        div.setAttribute("class", "col-md-12");
        div.innerHTML = `
            <div class="card" >
                <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                    <h4 class="title">
                        Você não possui projetos finalizados.
                    </h4>
                    <br/>
                </div>
            </div>
        `;
        projetosVazioFinalizado1.appendChild(div);
    }
    if(canceladoCont == 0){
        var div = document.createElement('div');
        div.setAttribute("class", "col-md-12");
        div.innerHTML = `
            <div class="card" >
                <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                    <h4 class="title">
                        Você não possui projetos cancelados.
                    </h4>
                    <br/>
                </div>
            </div>
        `;
        projetosVazioCancelado1.appendChild(div);
    }
}

function projetos(projeto){
    console.log('PROJETOS: ', projeto);
    let prof;
    fetch(api+'/api/user/perfil/'+projeto.profissional_id, {
        method: 'GET',
        headers: new Headers({
        'Accept': 'application/json'
        }),
    })
    .then((res) => {
        res.json()
        .then(certo =>{
            prof = certo;
        });
    }).catch((err) => {
        console.log(err)
    })

    if(projeto.length == 0){
        return `
            <div class="card" >
                <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                    <h4 class="title">
                        Você ainda não possui nenhum projeto.
                    </h4>
                    <br/>
                </div>
            </div>
        `
    }
    else{
        if(JSON.parse(window.localStorage.getItem('usuario')).perfil == 'profissional'){
            if(JSON.parse(window.localStorage.getItem('usuario')).id == projeto.profissional_id){
                if(projeto.tipo == "solicitacao" && projeto.status != "cancelado"){
                    return `
                        <div class="card" style="border: none" >
                            <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                                <h4 class="title">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <ul style="list-style:none !important; float: right; text-align: right; margin-right: 1%;" >
                                                <li style="text-decoration: none;" class="dropdown">
                                                    <ul class="dropdown-menu">
                                                        <li onclick="cancelarSolicitacao(${ projeto.id })">
                                                            <a href="#">Cancelar solicitação</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <br/>

                                            <label style="font-size: 1.2em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">${ projeto.nome_projeto } </label>
                                            <button type="button" rel="tooltip" title="" class="btn btn-success " data-original-title="Visualizar Proposta" onclick="verProposta()">
                                                <span>Ver proposta</span>
                                            </button>
                                            <br>
                                            <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Descricao: </label>
                                            <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.descricao_projeto } </span>
                                            <br>
                                            <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Contratante: </label>
                                            <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.nome_cliente } </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12"  style="float: rigth; text-align: right;">
                                            <button type="button" rel="tooltip" title="" class="btn btn-secondary" data-original-title="Recusar Solicitação" style="background-color: #888888; color: white;" onclick="cancelarSolicitacao(${ projeto.id })">
                                                <span>RECUSAR</span>
                                            </button>

                                            <button type="button" rel="tooltip" title="" class="btn btn-success "  style="background-color: #70b105; color: white;" data-original-title="Aceitar Solicitação" onclick="aceitarSolicitacao(${ projeto.id })">
                                                <span>ACEITAR</span>
                                            </button>
                                        </div>
                                    </div>
                                    <br/>
                                </h4>
                            </div>
                        </div>
                    `
                }
                else{
                    if(projeto.tipo == "atendimento" && projeto.status != "cancelado"){
                        return `
                            <div class="card" style="border: none" >
                                <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                                    <h4 class="title">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <ul style="list-style:none !important; float: right; text-align: right; margin-right: 1%;" >
                                                    <li style="text-decoration: none;" class="dropdown">
                                                        <ul class="dropdown-menu">
                                                            <li onclick="cancelarSolicitacao(${ projeto.id })">
                                                                <a href="#">Cancelar solicitação</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <br/>

                                                <label style="font-size: 1.2em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">${ projeto.nome_projeto } </label>
                                                <button type="button" rel="tooltip" title="" class="btn btn-success " data-original-title="Visualizar Proposta" onclick="verProposta()">
                                                    <span>Ver proposta</span>
                                                </button>
                                                <br>
                                                <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Descricao: </label>
                                                <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.descricao_projeto } </span>
                                                <br>
                                                <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Contratante: </label>
                                                <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.nome_cliente } </span>
                                            </div>
                                        </div>

                                        

                                        <div class="row">
                                            <div class="col-md-12 col-sm-12"  style="float: rigth; text-align: right;">
                                                <button type="button" rel="tooltip" title="" class="btn" data-original-title="Recusar Solicitação" style="background-color: #70b105 !important; color: white;" onclick="finalizar(${ projeto.id })">
                                                    <span>FINALIZAR</span>
                                                </button>
                                                
                                                <button type="button" rel="tooltip" title="" class="btn "  style="background-color: #70b105 !important; color: white;" data-original-title="Aceitar Solicitação" onclick="chat(${ projeto.cliente_id }, ${ projeto.profissional_id })">
                                                    <span>CHAT</span>
                                                </button>
                                                <button type="button" rel="tooltip" title="" class="btn "  style="background-color: #70b105 !important; color: white;" data-original-title="Contrato" onclick="contrato(${ projeto.id })">
                                                    <span>CONTRATO</span>
                                                </button>

                                                <button type="button" rel="tooltip" title="" class="btn "  style="background-color: #70b105 !important; color: white;" data-original-title="Acompanhamento do projeto" onclick="acompanharProjeto(${ projeto.cliente_id }, ${ projeto.profissional_id }, ${ projeto.id }, )">
                                                    <span>ACOMPANHAR</span>
                                                </button>
                                            </div>
                                        </div>
                                        <br/>
                                    </h4>
                                </div>
                            </div>
                        `
                    }
                    else{
                        if(projeto.tipo == "finalizado" &&  projeto.status != "cancelado"){
                            return  `
                                <div class="card"  >
                                    <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                                        <h4 class="title">
                                            <div class="row">
                                                <div class="col-md-12 col-sm-12">
                                                            
                                                    <label style="font-size: 1.2em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">${ projeto.nome_projeto } </label>
                                                    <button type="button" rel="tooltip" title="" class="btn btn-success " data-original-title="Visualizar Proposta" onclick="verProposta()">
                                                        <span>Ver proposta</span>
                                                    </button>
                                                    <br>
                                                    <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Descricao: </label>
                                                    <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.descricao_projeto } </span>
                                                    <br>
                                                    <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Contratante: </label>
                                                    <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.descricao_projeto } </span>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 col-sm-12"  style="float: rigth; text-align: right;">
                                                    <button type="button" rel="tooltip" title="" class="btn btn-secondary" data-original-title="Finalizada e paga" style="background-color: #888888; color: white;">
                                                        <span>Projeto Finalizado</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <br/>
                                        </h4>
                                    </div>
                                </div>
                            `
                        }
                        else{
                            if(projeto.status == 'cancelado'){
                                console.log('PROJETO ATUAL: ', projeto);
                                return  `
                                    <div class="card"  >
                                        <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                                            <h4 class="title">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <label style="font-size: 1.2em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">${ projeto.nome_projeto } </label>
                                                        <button type="button" rel="tooltip" title="" class="btn btn-success " data-original-title="Visualizar Proposta" onclick="verProposta()">
                                                            <span>Ver proposta</span>
                                                        </button>
                                                        <br>
                                                        <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Descricao: </label>
                                                        <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.descricao_projeto } </span>
                                                        <br>
                                                        <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Contratante: </label>
                                                        <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.nome_cliente } </span>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12"  style="float: rigth; text-align: right;">
                                                        <button type="button" rel="tooltip" title="" class="btn btn-danger" data-original-title="Finalizada e paga" style="background-color: #ef8484; color: white; border: none;">
                                                            <span>PROJETO CANCELADO</span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <br/>
                                            </h4>
                                        </div>
                                    </div>
                                `
                            }
                        }
                    }
                }
            }
        }
        else{
            if(JSON.parse(window.localStorage.getItem('usuario')).id == projeto.cliente_id){
                if(projeto.tipo == "solicitacao" && projeto.status != "cancelado"){
                    return `
                        <div class="card" style="border: none" >
                            <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                                <h4 class="title">
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12">
                                            <ul style="list-style:none !important; float: right; text-align: right; margin-right: 1%;" >
                                                <li style="text-decoration: none;" class="dropdown">
                                                    <ul class="dropdown-menu">
                                                        <li onclick="cancelarSolicitacao(${ projeto.id })">
                                                            <a href="#">Cancelar solicitação</a>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </ul>
                                            <br/>

                                            <label style="font-size: 1.2em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">${ projeto.nome_projeto } </label>
                                            <button type="button" rel="tooltip" title="" class="btn btn-success " data-original-title="Visualizar Proposta" onclick="verProposta()">
                                                <span>Ver proposta</span>
                                            </button>
                                            <br>
                                            <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Descricao: </label>
                                            <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.descricao_projeto } </span>
                                            <br>
                                            <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Contratante: </label>
                                            <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.nome_cliente } </span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12"  style="float: rigth; text-align: right;">
                                            <button type="button" rel="tooltip" title="" class="btn btn-success "  style="background-color: #87CB16; color: black; border: none;" data-original-title="Aguardando aceite do profissional" >
                                                <span>Aguardando aceite do profissional</span>
                                            </button>
                                        </div>
                                    </div>
                                    <br/>
                                </h4>
                            </div>
                        </div>
                    `
                }
                else{
                    if(projeto.tipo == "atendimento" && projeto.status != "cancelado"){
                        return `
                            <div class="card" style="border: none" >
                                <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                                    <h4 class="title">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <ul style="list-style:none !important; float: right; text-align: right; margin-right: 1%;" >
                                                    <li style="text-decoration: none;" class="dropdown">
                                                        <ul class="dropdown-menu">
                                                            <li onclick="cancelarSolicitacao(${ projeto.id })">
                                                                <a href="#">Cancelar solicitação</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <br/>

                                                <label style="font-size: 1.2em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">${ projeto.nome_projeto } </label>
                                                <button type="button" rel="tooltip" title="" class="btn btn-success " data-original-title="Visualizar Proposta" onclick="verProposta()">
                                                    <span>Ver proposta</span>
                                                </button>
                                                <br>
                                                <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Descricao: </label>
                                                <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.descricao_projeto } </span>
                                                <br>
                                                <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Contratante: </label>
                                                <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.nome_cliente } </span>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-md-12 col-sm-12"  style="float: rigth; text-align: right;">
                                                <button type="button" rel="tooltip" title="" class="btn" data-original-title="Recusar Solicitação" style="background-color: #70b105 !important; color: white;" onclick="finalizar(${ projeto.id })">
                                                    <span>FINALIZAR</span>
                                                </button>
                                                
                                                <button type="button" rel="tooltip" title="" class="btn "  style="background-color: #70b105 !important; color: white;" data-original-title="Aceitar Solicitação" onclick="chat(${ projeto.cliente_id }, ${ projeto.profissional_id })">
                                                    <span>CHAT</span>
                                                </button>
                                                <button type="button" rel="tooltip" title="" class="btn "  style="background-color: #70b105 !important; color: white;" data-original-title="Contrato" onclick="contrato(${ projeto.id })">
                                                    <span>CONTRATO</span>
                                                </button>

                                                <button type="button" rel="tooltip" title="" class="btn "  style="background-color: #70b105 !important; color: white;" data-original-title="Acompanhamento do projeto" onclick="acompanharProjeto(${ projeto.cliente_id }, ${ projeto.profissional_id }, ${ projeto.id }, )">
                                                    <span>ACOMPANHAR</span>
                                                </button>
                                            </div>
                                        </div>
                                        <!--
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12"  style="float: rigth; text-align: right;">
                                                <button type="button" rel="tooltip" title="" class="btn btn-secondary" data-original-title="Recusar Solicitação" style="background-color: #888888; color: white;" onclick="finalizar(${ projeto.id })">
                                                    <span>Finalizar</span>
                                                </button>

                                                <button type="button" rel="tooltip" title="" class="btn btn-success "  style="background-color: #87CB16; color: white;" data-original-title="Aceitar Solicitação" onclick="chat(${ projeto.cliente_id }, ${ projeto.profissional_id })">
                                                    <span>Chat</span>
                                                </button>

                                                <button type="button" rel="tooltip" title="" class="btn btn-info "  style="background-color: #1DC7EA; color: white;" data-original-title="Acompanhamento do projeto" onclick="acompanharProjeto(${ projeto.cliente_id }, ${ projeto.profissional_id }, ${ projeto.id }, )">
                                                    <span>Acompanhar</span>
                                                </button>
                                            </div>
                                        </div>
                                        -->
                                        <br/>
                                    </h4>
                                </div>
                            </div>
                        `
                    }
                    else{
                        if(projeto.tipo == "finalizado" &&  projeto.status != "cancelado"){
                            if(projeto.avaliacao == null){
                                return  `
                                    <div class="card"  >
                                        <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                                            <h4 class="title">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                                
                                                        <label style="font-size: 1.2em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial; text-align: center;">${ projeto.nome_projeto } </label>
                                                        <br/>
                                                        <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Descricao: </label>
                                                        <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.descricao_projeto } </span>
                                                        <br>
                                                        <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Profissional: </label>
                                                        <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${ projeto.nome_profissional } </span>
                                                    </div>
                                                </div>
                                                <p style="text-align: center;">Avalie <span style="font-weight: bolder;">${ projeto.nome_profissional }</span></p>
                                                <div class="row" style="text-align: center;">
                                                    <div class="col-md-12 col-sm-12">
                                                        <a href="#" style="text-align: center;" onclick="avaliacao(1)">
                                                            <i class="fa fa-heart" style="color: #00FF7F; font-size: 1.4em; display: none; text-align: center;" id="nota1"></i>
                                                        </a>  
                                                        <a href="#" style="text-align: center;" onclick="avaliacao(1)">
                                                            <i class="fa fa-heart" style="color: #c9cac9 font-size: 1.4em; display: initial; text-align: center;" id="nota11"></i>
                                                        </a>   
                                                        &nbsp; 
                                                        <a href="#" style="text-align: center;" onclick="avaliacao(2)">
                                                            <i class="fa fa-heart" style="color: #00FF7F; font-size: 1.4em; display: none; text-align: center;" id="nota2"></i>
                                                        </a>
                                                        <a href="#" style="text-align: center;" onclick="avaliacao(2)">
                                                            <i class="fa fa-heart" style="color: #c9cac9; font-size: 1.4em; display: initial; text-align: center;" id="nota21"></i>
                                                        </a>
                                                        &nbsp; 
                                                        <a href="#" style="text-align: center;" onclick="avaliacao(3)">
                                                            <i class="fa fa-heart" style="color: #00FF7F; font-size: 1.4em; display: none; text-align: center;" id="nota3"></i>
                                                        </a> 
                                                        <a href="#" style="text-align: center;" onclick="avaliacao(3)">
                                                            <i class="fa fa-heart" style="color: #c9cac9; font-size: 1.4em; display: initial; text-align: center;" id="nota31"></i>
                                                        </a> 
                                                        &nbsp;
                                                        <a href="#" style="text-align: center;" onclick="avaliacao(4)">
                                                            <i class="fa fa-heart" style="color: #00FF7F; font-size: 1.4em; display: none; text-align: center;" id="nota4"></i>
                                                        </a> 
                                                        <a href="#" style="text-align: center;" onclick="avaliacao(4)">
                                                            <i class="fa fa-heart" style="color: #c9cac9; font-size: 1.4em; display: initial; text-align: center;" id="nota41"></i>
                                                        </a> 
                                                        &nbsp;
                                                        <a href="#" style="text-align: center;" onclick="avaliacao(5)">
                                                            <i class="fa fa-heart" style="color: #00FF7F; font-size: 1.4em; display: none; text-align: center;" id="nota5"></i>
                                                        </a> 
                                                        <a href="#" style="text-align: center;" onclick="avaliacao(5)">
                                                            <i class="fa fa-heart" style="color: #c9cac9; font-size: 1.4em; display: initial; text-align: center;" id="nota51"></i>
                                                        </a>
                                                    </div>
                                                    <br/>
                                                    <button type="button" rel="tooltip" title="" class="btn btn-success "  style="text-align: center !important;" onclick="avaliarProfissional(${ projeto.id })">
                                                        <span>Avaliar</span>
                                                    </button>
                                                </div>
                                                <br/>
                                            </h4>
                                        </div>
                                    </div>
                                `
                            }
                            else{
                                return  `
                                    <div class="card"  >
                                        <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                                            <h4 class="title">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                                
                                                        <label style="font-size: 1.2em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">${ projeto.nome_projeto } </label>
                                                        <button type="button" rel="tooltip" title="" class="btn btn-success " data-original-title="Visualizar Proposta" onclick="verProposta()">
                                                            <span>Ver proposta</span>
                                                        </button>
                                                        <br>
                                                        <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Descricao: </label>
                                                        <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.descricao_projeto } </span>
                                                        <br>
                                                        <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Profissional: </label>
                                                        <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.nome_profissional } </span>
                                                    </div>
                                                </div>
                                                <div style="text-align: center;">
                                                    <p>Avaliação</p>
                                                    <p style="font-size: 1.4em;"> ${ projeto.avaliacao } <i class="fa fa-heart" style="color: #00FF7F; font-size: 1.1em;" ></i></p>
                                                </div>
                                                <br/>
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12"  style="float: rigth; text-align: right;">
                                                        <button type="button" rel="tooltip" title="" class="btn btn-secondary" data-original-title="Finalizada e paga" style="background-color: #888888; color: white;">
                                                            <span>Serviço Finalizado</span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <br/>
                                            </h4>
                                        </div>
                                    </div>
                                `
                            }
                        }
                        else{
                            if(projeto.status == 'cancelado'){
                                return  `
                                    <div class="card"  >
                                        <div class="header" style="border: 0.5px solid #8fe206; border-radius: 5px;">
                                            <h4 class="title">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12">
                                                        <label style="font-size: 1.2em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">${ projeto.nome_projeto } </label>
                                                        <button type="button" rel="tooltip" title="" class="btn btn-success " data-original-title="Visualizar Proposta" onclick="verProposta()">
                                                            <span>Ver proposta</span>
                                                        </button>
                                                        <br>
                                                        <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Descricao: </label>
                                                        <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.descricao_projeto } </span>
                                                        <br>
                                                        <label style="font-size: 0.7em; color: #000000; font-weight: 900; margin-bottom: -5%; text-transform: initial;">Contratante: </label>
                                                        <span style="font-size: 0.8em; margin-bottom: -8px; margin-top: -8px;">${projeto.descricao_projeto } </span>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12"  style="float: rigth; text-align: right;">
                                                        <button type="button" rel="tooltip" title="" class="btn btn-danger" data-original-title="Finalizada e paga" style="background-color: #ef8484; color: white; border: none;">
                                                            <span>Serviço Cancelado</span>
                                                        </button>
                                                    </div>
                                                </div>
                                                <br/>
                                            </h4>
                                        </div>
                                    </div>
                                `
                            }
                        }
                    }
                    
                }
            }
        }
    }
}

function showNotification(from, align) {
    type = ['', 'info', 'danger','success', 'warning', 'rose', 'primary'];

    color = Math.floor((Math.random() * 10) + 1);

    $.notify({
        icon: "notifications",
        message: "Por favor, forneça uma nota para avaliar o profissional."

    }, {
        type: type[color],
        timer: 3000,
        placement: {
            from: from,
            align: align
        }
    });
}

function avaliacao(nota){
    notaAvaliacao = nota;
    if(nota == 1){
        document.getElementById('nota1').style.display = 'initial';
        document.getElementById('nota11').style.display = 'none';

        document.getElementById('nota2').style.display = 'none';
        document.getElementById('nota21').style.display = 'initial';
        document.getElementById('nota3').style.display = 'none';
        document.getElementById('nota31').style.display = 'initial';
        document.getElementById('nota4').style.display = 'none';
        document.getElementById('nota41').style.display = 'initial';
        document.getElementById('nota5').style.display = 'none';
        document.getElementById('nota51').style.display = 'initial';
    }
    else if(nota == 2){
        document.getElementById('nota1').style.display = 'initial';
        document.getElementById('nota11').style.display = 'none';
        document.getElementById('nota2').style.display = 'initial';
        document.getElementById('nota21').style.display = 'none';

        document.getElementById('nota3').style.display = 'none';
        document.getElementById('nota31').style.display = 'initial';
        document.getElementById('nota4').style.display = 'none';
        document.getElementById('nota41').style.display = 'initial';
        document.getElementById('nota5').style.display = 'none';
        document.getElementById('nota51').style.display = 'initial';
    }
    else if(nota == 3){
        document.getElementById('nota1').style.display = 'initial';
        document.getElementById('nota11').style.display = 'none';
        document.getElementById('nota2').style.display = 'initial';
        document.getElementById('nota21').style.display = 'none';
        document.getElementById('nota3').style.display = 'initial';
        document.getElementById('nota31').style.display = 'none';

        document.getElementById('nota4').style.display = 'none';
        document.getElementById('nota41').style.display = 'initial';
        document.getElementById('nota5').style.display = 'none';
        document.getElementById('nota51').style.display = 'initial';
    }
    else if(nota == 4){
        document.getElementById('nota1').style.display = 'initial';
        document.getElementById('nota11').style.display = 'none';
        document.getElementById('nota2').style.display = 'initial';
        document.getElementById('nota21').style.display = 'none';
        document.getElementById('nota3').style.display = 'initial';
        document.getElementById('nota31').style.display = 'none';
        document.getElementById('nota4').style.display = 'initial';
        document.getElementById('nota41').style.display = 'none';

        document.getElementById('nota5').style.display = 'none';
        document.getElementById('nota51').style.display = 'initial';
    }
    else if(nota == 5){
        document.getElementById('nota1').style.display = 'initial';
        document.getElementById('nota11').style.display = 'none';
        document.getElementById('nota2').style.display = 'initial';
        document.getElementById('nota21').style.display = 'none';
        document.getElementById('nota3').style.display = 'initial';
        document.getElementById('nota31').style.display = 'none';
        document.getElementById('nota4').style.display = 'initial';
        document.getElementById('nota41').style.display = 'none';
        document.getElementById('nota5').style.display = 'initial';
        document.getElementById('nota51').style.display = 'none';
    }
}

function avaliarProfissional(id){
    let recebeDados;
    if(notaAvaliacao == 0){
        showNotification('top', 'right');
    }
    else{
        fetch(api+'/api/user/avaliacao/mostrar/'+id, {
            method: 'GET',
            headers: new Headers({
              'Accept': 'application/json',
              'Authorization': 'Bearer ' + window.localStorage.getItem('token')
            }),
        })
        .then((res) => {
            res.json()
            .then(avaliacao => {
                console.log('AVALIDADCADOAD: ', avaliacao);
                fetch(api+'/api/user/avaliacao/'+notaAvaliacao+'/'+avaliacao[0].id, {
                    method: 'POST',
                    headers: new Headers({
                      'Accept': 'application/json',
                      'Authorization': 'Bearer ' + window.localStorage.getItem('token')
                    }),
                })
                .then((res) => {
                    res.json()
                    .then(resultad => {
                        window.location.href = 'project.html';
                    });
                })
                .catch((err) => {
                    console.log(err)
                })
            });
        })
        .catch((err) => {
            console.log(err)
        })
    }
}

function apagarToken(){
    window.localStorage.clear();
}

function aceitarSolicitacao(id){
    fetch(api+'/api/solicitacao/atender/'+id, {
        method: 'GET',
        headers: new Headers({
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        }),
    })
    .then((res) => {
        res.json().then(body => {
            window.location.href = 'project.html';
        });
    })
    .catch((err) => {
        console.log(err)
    })
}

function cancelarSolicitacao(id){
    fetch(api+'/api/solicitacao/cancelar/'+id, {
        method: 'GET',
        headers: new Headers({
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        }),
    })
    .then((res) => {
        res.json().then(body => {
            window.location.href = 'project.html';
        });
    })
    .catch((err) => {
        console.log(err)
    })
}

function chat(id_cliente, id_profissional){
    var ids = {
        id_cliente: id_cliente,
        id_profissional: id_profissional
    }
    window.localStorage.setItem('ids', JSON.stringify(ids));
    window.location.href = 'chat.html';
}

function finalizar(id){
    fetch(api+'/api/solicitacao/finalizar/'+id, {
        method: 'GET',
        headers: new Headers({
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        }),
    })
    .then((res) => {
        res.json().then(body => {
            window.location.href = 'project.html';
        });
    })
    .catch((err) => {
        console.log(err)
    });
}

function verProposta(){
    alert('Posposta em Andamento');
}

function acompanharProjeto(id_profissional, id_cliente, id_solicitacao){
    let ids = {
        id_profissional: id_profissional,
        id_cliente: id_cliente,
        id_solicitacao: id_solicitacao
    }
    window.localStorage.setItem('idsCards', JSON.stringify(ids));
    window.location.href = 'acompanhamento.html';
}

function contrato(id_projeto){
    window.localStorage.setItem('id_projeto_contrato', id_projeto);
    window.location.href = 'contrato.html';
    console.log('CONTRATO: ', id_projeto);
}