
var api = "https://www.linkfor.com.br/admin/public";
// var api = "http://127.0.0.1:8000";
var dados;
var usuarios = document.getElementById('admin-recrutar');
document.getElementById('name-user').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).name;
var page = document.getElementById('paginacao');

fetch(api+'/api/recrutar/visualizar',{
    method: 'GET',
    headers: new Headers({
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    })
})
.then((profissionais)=>{
    profissionais.json()
    .then(certo => {
        dados = certo;
        for(let x = 0; x < certo.last_page; x++){
            var pag = document.createElement('li');
            pag.setAttribute("class", "page-item");
            pag.innerHTML = pagination(x);
            page.appendChild(pag);
        }

        certo.forEach(element => {
            var div = document.createElement('tr');
            div.innerHTML = projetos(element);
            usuarios.appendChild(div);
        });
    })
    .catch(erro => {
        console.log('DEU ERRADO:');
    })
})
.catch((err)=>{
    console.log(err)
})

function pagination (number){
    return `
        <a class="page-link active" onclick="prox(${ number+1 })" style="cursor: pointer;">
            ${ number+1 }
        </a>
    `
}


function prox(number){
    console.log('DADDA: ', number);
    fetch(dados.path+'?page=' + number,{
        method: 'GET',
        headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        })
    })
    .then(cdadad => {
        cdadad.json()
        .then(certo => {
            usuarios.innerHTML = '';
            certo.data.forEach(element => {
                var div = document.createElement('tr');
                div.innerHTML = projetos(element);
                usuarios.appendChild(div);
            });
        })
    })
}

function projetos(element){
    let nomeProfissional;
    if(element.nome_profissional == null)
        nomeProfissional = '';
    else
        nomeProfissional = element.nome_profissional;
    return `
        <tr >
            <td>
                #${ element.id }
            </td>
            <td>
                ${ element.name }
            </td>
            <td>
                ${ element.email }
            </td>
            <td>
                <a class="btn btn-success btn-fill btn-wd" onclick="showNotification('top','left')">
                    <i class="fa fa-envelope"></i>
                    Responder
                </a>
                <a class="btn btn-danger btn-fill btn-wd" onclick="excluirMensagem(${ element.id })">
                    <i class="fa fa-trash"></i>
                    Excluir
                </a>
            </td>
        </tr>
    `;
}

function showNotification(from, align) {
    type = ['', 'info', 'danger','success', 'warning', 'rose', 'primary'];

    color = Math.floor((Math.random() * 6) + 1);

    $.notify({
        icon: "notifications",
        message: "Recurso ainda não disponível. Aguarde, em breve."

    }, {
        type: type[color],
        timer: 3000,
        placement: {
            from: from,
            align: align
        }
    });
}


function excluirMensagem(id){
    console.log(id);
    fetch(api+'/api/contato/deletar/'+id, {
        method: 'DELETE',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        })
    })
    .then(sucesso => {
        sucesso.json()
        .then(Certo => {
            window.location.href = 'admin-contato.html';
        })
    })
    .catch(erro => {
        alert('Não foi possível excluir a mensagem');
        console.log('ERRO: ', erro);
    })
}