// var api = "http://127.0.0.1:8000";
var api = "https://www.linkfor.com.br/admin/public";

let nome = JSON.parse(window.localStorage.getItem('usuario')).name.split(' ');
document.getElementById('name-user').innerHTML = nome[0] + ' ' + nome[1];


function sair(){
    fetch(api+'/api/user/logout', {
      method: 'GET',
      headers: new Headers({
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + window.localStorage.getItem('token')
      }),
    })
    .then(logout => {
      logout.json()
      .then(certo => {
        window.localStorage.clear();
        window.location.href = '../index.html';
      })
      .catch(erro =>{
        notificacaoErro('top', 'right');
      })
    })
  }
  
  function notificacaoErro(from, align) {
    type = ['', 'info', 'danger','success', 'warning', 'rose', 'primary'];
  
    color = Math.floor((Math.random() * 10) + 1);
  
    $.notify({
      icon: "notifications",
      message: "Problema com nosso servidor, por favor tente mais tarde."
    }, 
    {
      type: type[color],
      timer: 3000,
      placement: {
        from: from,
        align: align
      }
    });
  }