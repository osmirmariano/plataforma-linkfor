
var api = "https://www.linkfor.com.br/admin/public";
// var api = "http://127.0.0.1:8000";

var dadosProfissionais, dadosClientes;
var profissionalL = document.getElementById('usuarioProfissional');
var usuariosClientes = document.getElementById('usuarioCliente');
document.getElementById('name-user').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).name;
var page1 = document.getElementById('paginacaoProf');
var page2 = document.getElementById('paginacaoClie');

fetch(api+'/api/user/todos-profissionais',{
    method: 'GET',
    headers: new Headers({
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    })
})
.then((profissionais)=>{
    profissionais.json()
    .then(certo => {
        dadosProfissionais = certo;
        for(let x = 0; x < certo.last_page; x++){
            var pag = document.createElement('li');
            pag.setAttribute("class", "page-item");
            pag.innerHTML = pagination(x);
            page1.appendChild(pag);
        }

        certo.data.forEach(element => {
            var div = document.createElement('tr');
            div.innerHTML = usuariosLinkfor(element);
            profissionalL.appendChild(div);
        });
    })
    .catch(erro => {
        console.log('DEU ERRADO: '. erro);
    })
})
.catch((err)=>{
    console.log(err)
})


fetch(api+'/api/user/todos-clientes',{
    method: 'GET',
    headers: new Headers({
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    })
})
.then((clientes)=>{
    clientes.json()
    .then(certo => {
        dadosClientes = certo;
        for(let x = 0; x < certo.last_page; x++){
            var pag = document.createElement('li');
            pag.setAttribute("class", "page-item");
            pag.innerHTML = pagination2(x);
            page2.appendChild(pag);
        }

        certo.data.forEach(element => {
            var div = document.createElement('tr');
            div.innerHTML = usuarios(element);
            usuariosClientes.appendChild(div);
        });
    })
    .catch(erro => {
        console.log('DEU ERRADO');
    })
})
.catch((err)=>{
    console.log(err)
})


function pagination (number){
    return `
        <a class="page-link active" onclick="prox(${ number+1 })" style="cursor: pointer;">
            ${ number+1 }
        </a>
    `
}

function pagination2 (number){
    return `
        <a class="page-link active" onclick="prox(${ number+1 })" style="cursor: pointer;">
            ${ number+1 }
        </a>
    `
}

function prox(number){
    fetch(dadosProfissionais.path+'?page=' + number,{
        method: 'GET',
        headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        })
    })
    .then(cdadad => {
        cdadad.json()
        .then(certo => {
            console.log('12345', certo);
            profissionalL.innerHTML = '';
            certo.data.forEach(element => {
                var div = document.createElement('tr');
                div.innerHTML = usuariosLinkfor(element);
                profissionalL.appendChild(div);
            });
        })
    })
}

function proxCliente(number){
    fetch(dadosClientes.path+'?page=' + number,{
        method: 'GET',
        headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        })
    })
    .then(cdadad => {
        cdadad.json()
        .then(certo => {
            usuariosClientes.innerHTML = '';
            certo.data.forEach(element => {
                var div = document.createElement('tr');
                div.innerHTML = usuarios(element);
                usuariosClientes.appendChild(div);
            });
        })
    })
}

function usuariosLinkfor(element){
    let area;
    if(element.area == null)
        area = '';
    else
        area = element.area;
    return `
        <tr data-toggle="modal" data-target=".bd-example-modal-lg">
            <td>
                #${ element.id }
            </td>

            <td>
                ${ element.name }
            </td>

            <td>
                ${ area }
            </td>

            <td>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bd-example-modal-lg" onclick="visualizar(${ element.id  })">
                    Visualizar
                </button>
            </td>
        </tr>
    `;
}

function usuarios(element){
    return `
        <tr >
            <td>
                #${ element.id }
            </td>

            <td>
                ${ element.name }
            </td>
        </tr>
    `;
}

function visualizar(id){
    console.log('Usuário Selecionado: ', id);
    fetch(api+'/api/user/perfil/' + id,{
        method: 'GET',
        headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        })
    })
    .then(cdadad => {
        cdadad.json()
        .then(certo => {
            document.getElementById('nome').innerHTML = certo.name;
            document.getElementById('email').innerHTML = certo.email;
            document.getElementById('dd').innerHTML = certo.dd;
            document.getElementById('telefone').innerHTML = certo.telefone;
            document.getElementById('area').innerHTML = certo.area;
            document.getElementById('descricao').innerHTML = certo.descricao;
            document.getElementById('universidade').innerHTML = certo.college;
            document.getElementById('curso').innerHTML = certo.course;
            document.getElementById('dataCriacao').innerHTML = certo.created_at;
            document.getElementById('experiencia').innerHTML = certo.experiencia_profissional;
            document.getElementById('especialidade').innerHTML = certo.especialidades;
            document.getElementById('status').innerHTML = certo.situacao;
            document.getElementById('portfolio').innerHTML = certo.link_portfolio;
            document.getElementById('linkdin').innerHTML = certo.link_linkedin;
            document.getElementById('github').innerHTML = certo.link_github;
            document.getElementById('formacao').innerHTML = certo.formacao;
            document.getElementById('melhor_habilidade').innerHTML = certo.melhor_habilidade;
            document.getElementById('tempo_experiencia').innerHTML = certo.tempo_experiencia;
            document.getElementById('melhor_projeto').innerHTML = certo.melhor_projeto;
            document.getElementById('link_melhor_projeto').innerHTML = certo.link_melhor_projeto;
            document.getElementById('experiencia_remoto').innerHTML = certo.experiencia_remoto;
            document.getElementById('organizacao_tempo').innerHTML = certo.organizacao_tempo;
            document.getElementById('ambiente_ideal').innerHTML = certo.ambiente_ideal;
        })
    })
}
