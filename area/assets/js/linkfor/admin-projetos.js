
var api = "https://www.linkfor.com.br/admin/public";
// var api = "http://127.0.0.1:8000";
var dados;
var usuarios = document.getElementById('projetos');
document.getElementById('name-user').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).name;
var page = document.getElementById('paginacao');

fetch(api+'/api/solicitacao/listar-solicitacao',{
    method: 'GET',
    headers: new Headers({
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    })
})
.then((profissionais)=>{
    profissionais.json()
    .then(certo => {
        dados = certo;
        for(let x = 0; x < certo.last_page; x++){
            var pag = document.createElement('li');
            pag.setAttribute("class", "page-item");
            pag.innerHTML = pagination(x);
            page.appendChild(pag);
        }

        certo.forEach(element => {
            var div = document.createElement('tr');
            div.innerHTML = projetos(element);
            usuarios.appendChild(div);
        });
    })
    .catch(erro => {
        console.log('DEU ERRADO:');
    })
})
.catch((err)=>{
    console.log(err)
})

function pagination (number){
    return `
        <a class="page-link active" onclick="prox(${ number+1 })" style="cursor: pointer;">
            ${ number+1 }
        </a>
    `
}


function prox(number){
    console.log('DADDA: ', number);
    fetch(dados.path+'?page=' + number,{
        method: 'GET',
        headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        })
    })
    .then(cdadad => {
        cdadad.json()
        .then(certo => {
            usuarios.innerHTML = '';
            certo.data.forEach(element => {
                var div = document.createElement('tr');
                div.innerHTML = projetos(element);
                usuarios.appendChild(div);
            });
        })
    })
}

function projetos(element){
    let nomeProfissional;
    if(element.nome_profissional == null)
        nomeProfissional = '';
    else
        nomeProfissional = element.nome_profissional;
    return `
        <tr >
            <td>
                #${ element.id }
            </td>
            <td>
                ${ element.nome_projeto }
            </td>
            <td>
                ${ element.nome_cliente }
            </td>
            <td>
                ${ nomeProfissional }
            </td>
            <td>
                ${ element.tipo }
            </td>
            <td>
                ${ element.status }
            </td>
        </tr>
    `;
}