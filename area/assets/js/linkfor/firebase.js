 // Initialize Firebase
 var config = {
    apiKey: "AIzaSyDkHW76NSEZPHU7pa1Xiau2wtuch6bRNPY",
    authDomain: "linkfor-b1724.firebaseapp.com",
    databaseURL: "https://linkfor-b1724.firebaseio.com",
    projectId: "linkfor-b1724",
    storageBucket: "linkfor-b1724.appspot.com",
    messagingSenderId: "920840823334"
};
firebase.initializeApp(config);

var db = firebase.database();
var selectedFile;

$( document ).ready(function() {
	$("#welcome").hide();
	$(".upload-group").hide();
	document.getElementById("upload").addEventListener('change', handleFileSelect, false);
});

var reviewForm = document.getElementById('reviewForm');
var fullName   = document.getElementById('fullName');
var message    = document.getElementById('message');
var hiddenId   = document.getElementById('hiddenId');


var data = new Date();
var dia = data.getDate();
var mes = data.getMonth() + 1;
var ano = data.getFullYear();
var horas = new Date().getHours();
var minutos = new Date().getMinutes();
var segundos = new Date().getSeconds();
var resultado = dia + "-" + mes + "-" + ano + " - " + horas + ":" + minutos;

reviewForm.addEventListener('submit', (e) => {
    e.preventDefault();

    if (!message.value) return null
    var id = hiddenId.value || Date.now()
    db.ref('chat/'+JSON.parse(window.localStorage.getItem('ids')).id_profissional+''+ JSON.parse(window.localStorage.getItem('ids')).id_cliente).push({
        id: JSON.parse(window.localStorage.getItem('usuario')).id,
        nome: JSON.parse(window.localStorage.getItem('usuario')).name,
        data: resultado,
        mensagem: message.value
    })
    fullName = '';
    message.value  = '';
    hiddenId.value = '';
});

var sala = document.getElementById('sala');
var salaRef = db.ref('/chat/');

salaRef.on('child_added', (data) => {
    var li = document.createElement('li')
    li.id = data.key;
    li.innerHTML = mostrarSala(data)
    sala.appendChild(li);
});

function mostrarSala(dados){
    var recebe = dados.key;
    var vetor = recebe.split('-'), x;
    for(x = 0; x < JSON.parse(window.localStorage.getItem('projetos')).length; x++){
        if(vetor[0] == JSON.parse(window.localStorage.getItem('projetos'))[x].profissional_id){
            return ` 
                <a href="#">
                    <div style="margin-top: 8px; padding-right: 5%;">
                        <div 
                            style=
                            "
                                box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); 
                                transition: 0.3s; 
                                padding: 5%;
                                text-decoration:none; 
                                background: #BBDEFB;
                                padding: 10px;
                                position: relative;
                                width: 100%;
                                padding-right: 5%;
                            ">
                            <img src="./assets/img/faces/face-0.jpg" style="border-radius: 100px; width: 10%;"/>
                            <p style="position: relative; float: right; font-size: 1.5em; margin-top: 3%;"> ${ JSON.parse(window.localStorage.getItem('projetos'))[x].nome_cliente } </p>            
                        </div>
                    </div>
                </a>
            `
        }
        else if(vetor[1] == JSON.parse(window.localStorage.getItem('projetos'))[x].cliente_id){
            return ` 
                <div style="margin-top: 8px; padding-right: 5%;">
                    <div 
                        style=
                        "
                            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); 
                            transition: 0.3s; 
                            padding: 5%;
                            text-decoration:none; 
                            background: #BBDEFB;
                            padding: 10px;
                            position: relative;
                            border-radius: 20px 20px 20px 20px;
                            width: 100%;
                            padding-right: 5%;
                        "
                    >
                    <img src="./assets/img/faces/face-0.jpg" style="border-radius: 100px; width: 10%;"/>
                    <p style="position: relative; float: right;"> ${ JSON.parse(window.localStorage.getItem('projetos'))[x].nome_cliente } </p>                       
                    </div>
                </div>
            `
        }
    }

    
}

var reviews = document.getElementById('reviews');
var reviewsRef = db.ref('/chat/'+JSON.parse(window.localStorage.getItem('ids')).id_profissional+''+ JSON.parse(window.localStorage.getItem('ids')).id_cliente);

reviewsRef.on('child_added', (data) => {
    var li = document.createElement('li')
    li.id = data.key;
    li.innerHTML = reviewTemplate(data.val())
    reviews.appendChild(li);
});

reviewsRef.on('child_changed', (data) => {
    var reviewNode = document.getElementById(data.key);
    reviewNode.innerHTML = reviewTemplate(data.val());
});

reviewsRef.on('child_removed', (data) => {
    var reviewNode = document.getElementById(data.key);
    reviewNode.parentNode.removeChild(reviewNode);
});

reviews.addEventListener('click', (e) => {
    var reviewNode = e.target.parentNode
    // UPDATE REVEIW
    if (e.target.classList.contains('edit')) {
        message.value  = reviewNode.querySelector('.message').innerText;
        hiddenId.value = reviewNode.id;
    }

    // DELETE REVEIW
    if (e.target.classList.contains('delete')) {
        var id = reviewNode.id;
        db.ref('/chat/'+JSON.parse(window.localStorage.getItem('ids')).id_profissional+''+ JSON.parse(window.localStorage.getItem('ids')).id_cliente).remove();
    }
});

function reviewTemplate(dados) {
    var element = document.getElementById("reviews");
    element.scrollTop = 30000;
    // var timerId = setInterval(element, 500);
    
    // setTimeout(function () {
    //     clearInterval(timerId)
    // }, 10000);

    if(dados.url == null){
        if(JSON.parse(window.localStorage.getItem('usuario')).id == dados.id){
            return `
                <div style="margin-top: 8px; padding-right: 5%;">
                    <div 
                        style=
                        "
                            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); 
                            transition: 0.3s; 
                            padding: 5%;
                            text-decoration:none; 
                            background: #BBDEFB;
                            padding: 10px;
                            position: relative;
                            border-radius: 20px 0px 20px 20px;
                            min-width: 100px;
                            max-width: 95%;
                            float: right;
                            padding-right: 5%;
                            white-space: normal;
                        "
                    >
                        <p style="color: #02db02; font-style: italic; font-size: 1em; font-weight: 700;"> ${dados.nome} </p>
                        <p style="word-wrap: break-word;" > ${dados.mensagem} </p>
                        <p style="color: gray; font-style: italic; font-size: 1em; text-align: right;"> ${dados.data} </p>            
                    </div>
                    <br/>
                </div>
                `
        }
        else{
            return `
                <div style=" margin-top: 8px; padding-right: 5%;">
                    <div 
                        style=
                        "
                            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); 
                            transition: 0.3s; 
                            padding: 5%;
                            text-decoration:none; 
                            background: #ececec;
                            padding: 10px;
                            position: relative;
                            border-radius: 0px 20px 20px 20px;
                            min-width: 100px;
                            max-width: 95%;
                            float: left;
                            white-space: normal;
                        "
                    >
                        <p style="color: #02db02; font-style: italic; font-size: 1em; font-weight: 700;"> ${dados.nome} </p>
                        <p style="word-wrap: break-word;"> ${dados.mensagem} </p>
                        <p style="color: gray; font-style: italic; font-size: 1em; text-align: right;"> ${dados.data} </p>            
                    </div>
                    <br/>
                </div>
            `
        }
    }
    else{
        if(JSON.parse(window.localStorage.getItem('usuario')).id == dados.id){
            return `
                <div style="margin-top: 8px; padding-right: 5%;">
                    <div 
                        style=
                        "
                            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); 
                            transition: 0.3s; 
                            padding: 5%;
                            text-decoration:none; 
                            background: #BBDEFB;
                            padding: 10px;
                            position: relative;
                            border-radius: 20px 0px 20px 20px;
                            min-width: 100px;
                            max-width: 95%;
                            float: right;
                            padding-right: 5%;
                        "
                    >
                        <p style="color: #02db02; font-style: italic; font-size: 1em; font-weight: 700;"> ${dados.nome} </p>
                        <img src="${dados.url}" style="min-width: 100px;max-width: 700px; float: left; position: relative;">
                        <br>
                        <p style="color: gray; font-style: italic; font-size: 1em; text-align: right;"> ${dados.data} </p>            
                    </div>
                    <br/>    
                </div>
            `
        }
        else{
            return `
                <div style="margin-top: 8px; padding-right: 5%;">
                    <div 
                        style=
                        "
                            box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2); 
                            transition: 0.3s; 
                            padding: 5%;
                            text-decoration:none; 
                            background: #ececec;
                            padding: 10px;
                            position: relative;
                            border-radius: 0px 20px 20px 20px;
                            min-width: 100px;
                            max-width: 95%;
                            float: left;
                        "
                    >
                        <p style="color: #02db02; font-style: italic; font-size: 1em; font-weight: 700;"> ${dados.nome} </p>
                        <img src="${dados.url}" style="min-width: 100px; max-width: 700px; float: left; position: relative; overflow-y: auto;
                        margin-bottom: 10px;
                        height: calc(100% - 80px);">
                        <br>
                        <p style="color: gray; font-style: italic; font-size: 1em; text-align: right;"> ${dados.data} </p>            
                    </div>
                    <br/>
                </div>
            `
        }
    }
};

function handleFileSelect(event) {
	$(".upload-group").show();
	selectedFile = event.target.files[0];
};

function confirmUpload() {
    var nomeArquivo = selectedFile.name;
    var storageRef = firebase.storage().ref('/linkfor/' + nomeArquivo);    
    var uploadTask = storageRef.put(selectedFile);
    var downloadURL;
	uploadTask.on('state_changed', function(snapshot){

    }, 
    function(error) {
        
    }, 
    function() {
        downloadURL = uploadTask.snapshot.ref.location.bucket +'/'+uploadTask.snapshot.ref.location.path;
        var storage2 = firebase.storage();
        var starsRef = storage2.refFromURL('gs://'+downloadURL);

        // Get the download URL
        starsRef.getDownloadURL()
        .then(function(url) {
            db.ref('chat/'+JSON.parse(window.localStorage.getItem('ids')).id_profissional+''+ JSON.parse(window.localStorage.getItem('ids')).id_cliente).push({
                id: JSON.parse(window.localStorage.getItem('usuario')).id,
                nome: JSON.parse(window.localStorage.getItem('usuario')).name,
                data: resultado,
                url: url,
            }).key;
        })
        .catch(function(error) {


        switch (error.code) {
            case 'storage/object_not_found':
            break;

            case 'storage/unauthorized':
            break;

            case 'storage/canceled':               
            break;

        }
        });

  		$(".upload-group")[0].before("Success!");
  		$(".upload-group").hide();

	});
    
}