var api = "https://www.linkfor.com.br/admin/public";
// var api = "http://127.0.0.1:8000";
profissionalPesquisar = document.getElementById('usuarioProfissional');
let dadosValores;
var page = document.getElementById('paginacao');
document.getElementById('name-user').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).name;

fetch(api+'/api/user/todos-profissionais',{
    method: 'GET',
    headers: new Headers({
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    })
})
.then((profissionais)=>{
    profissionais.json()
    .then(certo => {
        dadosValores = certo;
        for(let x = 0; x < certo.last_page; x++){
            var pag = document.createElement('li');
            pag.setAttribute("class", "page-item");
            pag.innerHTML = pagination(x);
            page.appendChild(pag);
        }

        certo.data.forEach(element => {
            if(element.situacao != 'bloqueado'){
                var div = document.createElement('div');
                div.setAttribute("class", "col-md-12 col-lg-3 col-sm-3 col-xl-3");
                div.innerHTML = profissionaisLinkfor(element);
                profissionalPesquisar.appendChild(div);
            }
        });
    })
    .catch(erro => {
        console.log('DEU ERRADO: '. erro);
    })
})
.catch((err)=>{
    console.log(err)
})

function pagination (number){
    return `
        <a class="page-link active" onclick="prox(${ number+1 })" style="cursor: pointer;">
            ${ number+1 }
        </a>
    `
}

function prox(number){
    fetch(dadosValores.path+'?page=' + number,{
        method: 'GET',
        headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        })
    })
    .then(cdadad => {
        cdadad.json()
        .then(certo => {
            profissionalPesquisar.innerHTML = '';
            certo.data.forEach(element => {
                if(element.situacao != 'bloqueado'){
                    var div = document.createElement('div');
                    div.setAttribute("class", "col-md-12 col-lg-3 col-sm-3 col-xl-3");
                    div.innerHTML = profissionaisLinkfor(element);
                    profissionalPesquisar.appendChild(div);
                }
            });
        })
    })
}

function profissionaisLinkfor(element){
    console.log(element);
    if(element.area == 'desenvolvedor_web') {
        dados = 'Desenvolvedor Web';
    }
    if(element.area == 'desenvolvedor_mobile') {
        dados = 'Desenvolvedor Mobile';
    }
    if(element.area == 'designer') {
        dados = 'Designer';
    }
    if(element.area == 'desenvolvedor_web,desenvolvedor_mobile') {
        dados = 'Desenvolvedor Web e Desenvolvedor Mobile';
    }
    if(element.area == 'desenvolvedor_web,desenvolvedor_mobile,designer') {
        dados = 'Desenvolvedor Web, Desenvolvedor Mobile e Designer';
    }

    return `
        <div class="card mb-3 shadow-sm" style="border-radius: 10px;">
            <br/>
            <img class="card-img-top" style="display: block;margin-left: auto;margin-right: auto; border-radius: 50%;overflow: hidden;width: 70px;height: 70px; margin-top: 5%;" src="../assets/img/user.svg">
            <div class="card-body" style="text-align: center !important;">
                <h6  style="font-size: 1em; font-weight: bolder; color: #426941; text-align: center;">${ element.name }</h6>
                <p style="font-size: 1em; font-weight: bolder; color: rgb(168, 168, 168); text-align: center;">${ dados }</p>
                
                <div class="d-flex justify-content-between align-items-center" style="margin-top: 20%; margin: 0 auto !important;">
                    <div class="btn-group" style="margin: 0 auto !important; padding: 5%;">
                        <button type="button" class="btn" style="background-color: #426941 !important; color: white; margin: 0 auto !important; border: none;">
                            <strong onclick="chamar(${ element.id })">NEGOCIAR</strong>
                        </button>
                    </div>
                </div>                
        </div>
    `
}

function chamar(id){
    window.localStorage.setItem('solicitacao', id);
    window.location.href = '../servicos.html';
}

function visualPerfil(id){
    perfilProfissional.forEach(element => {
        if(element.id == id){
            window.localStorage.setItem('perfil', JSON.stringify(element));
            window.location.href = 'perfil.html';
        }
    });
}

function pesquisarPorNome(){
    notificacaoErroPesquisar('top', 'right');
}


function notificacaoErroPesquisar(from, align) {
    type = ['', 'info', 'danger','success', 'warning', 'rose', 'primary'];
  
    color = Math.floor((Math.random() * 10) + 1);
  
    $.notify({
      icon: "notifications",
      message: "Serviço ainda não disponível."
    }, 
    {
      type: type[color],
      timer: 3000,
      placement: {
        from: from,
        align: align
      }
    });
}