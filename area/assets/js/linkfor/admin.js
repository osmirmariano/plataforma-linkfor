
var api = "https://www.linkfor.com.br/admin/public";
// var api = "http://127.0.0.1:8000";
document.getElementById('name-user').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).name;
var dados;
var usuarios = document.getElementById('admin');
var page = document.getElementById('paginacao');

fetch(api+'/api/user/todos-profissionais',{
    method: 'GET',
    headers: new Headers({
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    })
})
.then((profissionais)=>{
    profissionais.json()
    .then(certo => {
        console.log('CERTO: ', certo);
        dados = certo;
        // page = certo.last_page;
        for(let x = 0; x < certo.last_page; x++){
            var pag = document.createElement('li');
            pag.setAttribute("class", "page-item");
            pag.innerHTML = pagination(x);
            page.appendChild(pag);
        }
        certo.data.forEach(element => {
            var div = document.createElement('tr');
            div.innerHTML = ativaDesativar(element);
            usuarios.appendChild(div);
        });
    })
    .catch(erro => {
        console.log('DEU ERRADO');
    })
})
.catch((err)=>{
    console.log(err)
})

function pagination (number){
    return `
        <a class="page-link active" onclick="prox(${ number+1 })" style="cursor: pointer;">
            ${ number+1 }
        </a>
    `
}

function prox(number){
    console.log('DADDA: ', number);
    fetch(dados.path+'?page=' + number,{
        method: 'GET',
        headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        })
    })
    .then(cdadad => {
        console.log('12345', cdadad);
        cdadad.json()
        .then(certo => {
            usuarios.innerHTML = '';
            certo.data.forEach(element => {
                var div = document.createElement('tr');
                div.innerHTML = ativaDesativar(element);
                usuarios.appendChild(div);
            });
        })
    })
}

function ativaDesativar(element){
    if(element.situacao == "bloqueado"){
        return `
            <tr >
                <td>
                   #${ element.id }
                </td>
                <td>
                    ${ element.name }
                </td>
                <td>
                    ${ element.situacao }
                </td>
                <td>
                    <a class="btn btn-success btn-fill btn-wd" onclick="ativarBloquear(${ element.id })">
                        Ativar
                    </a>
                </td>
            </tr>
        `;
    }
    else{
        return `
            <tr >
                <td>
                    #${ element.id }
                </td>
                <td>
                    ${ element.name }
                </td>
                <td>
                    ${ element.situacao }
                </td>
                <td>
                    <a class="btn btn-danger btn-fill btn-wd" onclick="ativarBloquear(${ element.id })">
                        Bloquear
                    </a>
                </td>
            </tr>
        `;
    }
}

function ativarBloquear(id){
    console.log('ID: ', id);
    fetch(api+'/api/user/situacao/'+id,{
        method: 'POST',
        headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        })
    })
    .then((profissionais)=>{
        profissionais.json()
        .then(certo => {
            console.log('CERTO: ', certo);
            window.location.href = './admin-blqueio.html';
        })
        .catch(erro => {
            console.log('DEU ERRADO');
        })
    })
    .catch((err)=>{
        console.log(err)
    })
}