// var api = "http://127.0.0.1:8000";
var api = "https://www.linkfor.com.br/admin/public";

if(JSON.parse(window.localStorage.getItem('usuario')).perfil == 'profissional'){
    document.getElementById('criar_quadro').style.display = 'block';
}
else{
    document.getElementById('criar_quadro').style.display = 'none';
}

let  usarioProjetoCard = document.getElementById('cardUsuario');

if(JSON.parse(window.localStorage.getItem('usuario')).perfil == 'profissional'){
    fetch(api+'/api/projeto/mostrar/'+JSON.parse(window.localStorage.getItem('idsCards')).id_profissional, {
        method: 'GET',
        headers: new Headers({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        }),
    })
    .then((res) => {
        res.json().then(certo => {
            console.log('DADD: ', certo);
            if(certo.data.length == 0){
                var div = document.createElement('div');
                div.setAttribute("class", "col-md-12");
                div.innerHTML = 
                    `
                        <div class="card mb-4 shadow-sm">
                            <div style="text-align: center; padding: 10px;">
                                <p>
                                    Você ainda não criou nenhum quadro para seu projeto.
                                </p>                    
                            </div>
                        </div>
                    `
                ;
                usarioProjetoCard.appendChild(div);
            }
            else{
                certo.data.forEach(element => {
                    var div = document.createElement('div');
                    div.setAttribute("class", "col-md-6 col-lg-4 col-sm-12 col-xl-4");
                    div.innerHTML = cardProfissional(element);
                    usarioProjetoCard.appendChild(div);
                });
            }
        });
    })
    .catch((err) => {
        console.log(err)
    });
}
else{
    fetch(api+'/api/projeto/mostrar-cliente/'+JSON.parse(window.localStorage.getItem('idsCards')).id_cliente, {
        method: 'GET',
        headers: new Headers({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        }),
    })
    .then((res) => {
        res.json().then(certo => {
            if(certo.data.length == 0){
                var div = document.createElement('div');
                div.setAttribute("class", "col-md-12");
                div.innerHTML =  
                    `
                        <div class="card mb-4 shadow-sm">
                            <div style="text-align: center; padding: 10px;">
                                <p>
                                    O profissional ainda não criou nenhum quadro para seu projeto.
                                </p>                    
                            </div>
                        </div>
                    `
                ;
                usarioProjetoCard.appendChild(div);
            }
            else{
                certo.data.forEach(element => {
                    var div = document.createElement('div');
                    div.setAttribute("class", "col-md-6 col-lg-4 col-sm-12 col-xl-4");
                    div.innerHTML = cardCliente(element);
                    usarioProjetoCard.appendChild(div);
                });
            }
        });
    })
    .catch((err) => {
        console.log(err)
    });
}


fetch(api+'/api/projeto/visualizar/'+JSON.parse(window.localStorage.getItem('idsCards')).id_solicitacao, {
    method: 'GET',
    headers: new Headers({
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    }),
})
.then((res) => {
    res.json().then(certo => {
        console.log('CETO: ', certo);
        document.getElementById('userCliente').innerHTML = certo.cliente.name;
        document.getElementById('userProfissional').innerHTML = certo.profissional.name;
    });
})
.catch((err) => {
    console.log(err)
});

function cardProfissional(element){
    console.log('ELEMNT: ', element);
    return `
        <div class="card mb-4 shadow-sm" id="projetosMostra${ element.id }" style="display: block;">
            <br/>
            <div class="" style="text-align: center;">
                <div class="" style="margin: 0 auto; text-align: center; padding: 2px;">
                    <button type="button" class="btn btn-light" style=" color: black; marign: 0 auto; text-align: center;"  title="Editar Card" onclick="editar(${ element.id })">
                        <i class="pe-7s-note" style="font-size: 1.3em; "></i>    
                    </button>
                    <button type="button" class="btn btn-danger" style=" color: black; marign: 0 auto; text-align: center;" title="Deletar Card" onclick="deletar(${ element.id })">
                        <i class="pe-7s-trash" style="font-size: 1.3em; color: #FF4A55;"></i>    
                    </button>                    
                </div>
            </div>   
            <hr/>
            <div class="card-body" style="padding: 5%;">
                <h6  style="font-size: 1.1em; font-weight: bolder; color: #00C74E; text-align: justify; padding-bottom: 8px;">
                    ${ element.titulo }
                </h6>
                <h6  style="font-size: 1.1em; font-weight: 300; color: black; text-align: justify; padding-bottom: 8px; text-transform: none !important;">
                    ${ element.descricao }
                </h6>
                <br/>
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
                        <p style="text-align: left; font-size: .9em; margin-bottom: -2px;">Data Criação:</p>    
                        <p style="font-size: 1em; font-weight: bolder; color: rgb(168, 168, 168); text-align: left;">
                            <i class="pe-7s-stopwatch"></i> 
                            ${ element.data_criacao }
                        </p>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
                        <p style="text-align: right; font-size: .9em; margin-bottom: -2px;">Data Finalização:</p>
                        <p style="font-size: 1em; font-weight: bolder; color: rgb(168, 168, 168); text-align: right;">
                            <i class="pe-7s-timer"></i> 
                            ${ element.data_finalizacao }
                        </p>
                    </div>
                </div>
                <br/>             
            </div>
        </div>


        <div class="card mb-4 shadow-sm" id="projetosEditar${ element.id }" style="display: none">
            <br/>
            
            <div class="card-body" style="padding: 5%;">
                <form action="javascript:atualizar(${ element.id })" method="POST">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Titulo:</label>
                        <input type="text" class="form-control" id="titulo${ element.id }" value="${ element.titulo }">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Descrição</label>
                        <textarea type="text" class="form-control" id="descricao${ element.id }">${ element.descricao }</textarea>
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Data Criação:</label>
                        <input type="date" class="form-control" id="data_criacao${ element.id }" value="${ element.data_criacao }">
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">Data Finalização:</label>
                        <input type="date" class="form-control" id="data_finalizacao${ element.id }" value="${ element.data_finalizacao }">
                    </div>

                    <button type="submit" class="btn btn-success" style=" color: black; marign: 0 auto; text-align: right;" title="Atualizar Card">
                        <i class="pe-7s-diskette" style="font-size: 1.3em; "></i>  
                        Atualizar  
                    </button>
                    <button type="button" class="btn btn-danger" style=" color: black; marign: 0 auto; text-align: right;" title="Cancelar Edição Card" onclick="cancelar(${ element.id })">
                        <i class="pe-7s-shield" style="font-size: 1.3em; "></i>  
                        Cancelar
                    </button>
                </form>
                <br/>             
            </div>
        </div>
    `
}

function atualizar(id){
    console.log('ID: 1', id);
    console.log('ID 2: ', document.getElementById('titulo'+id).value);
    console.log('ID 3: ', document.getElementById('descricao'+id).value);
    console.log('ID 4: ', document.getElementById('data_criacao'+id).value);
    console.log('ID 5: ', document.getElementById('data_finalizacao'+id).value);
    fetch(api+'/api/projeto/editar/'+id, {
        method: 'PUT',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        }),
        body:
        'titulo='+document.getElementById('titulo'+id).value+
        '&descricao='+document.getElementById('descricao'+id).value+
        '&data_criacao='+document.getElementById('data_criacao'+id).value+
        '&data_finalizacao='+document.getElementById('data_finalizacao'+id).value
    })
    .then(sucesso => {
        sucesso.json()
        .then(Certo => {
            console.log('SUCESSO: ', Certo);
            window.location.href = 'acompanhamento.html';
        })
    })
    .catch(erro => {
        console.log('ERRO: ', erro);
    })
}

function editar(id){
    document.getElementById('projetosEditar'+id).style.display = 'block';
    document.getElementById('projetosMostra'+id).style.display = 'none';
}

function cancelar(id){
    document.getElementById('projetosEditar'+id).style.display = 'none';
    document.getElementById('projetosMostra'+id).style.display = 'block';
}

function cardCliente(element){
    return `
        <div class="card mb-4 shadow-sm" >
            <br/>
            <div class="card-body" style="padding: 5%;">
                <h6  style="font-size: 1.1em; font-weight: bolder; color: #00C74E; text-align: justify; padding-bottom: 8px;">
                    ${ element.titulo }
                </h6>
                <h6  style="font-size: 1.1em; font-weight: 300; color: black; text-align: justify; padding-bottom: 8px; text-transform: none !important;">
                    ${ element.descricao }
                </h6>
                <br/>
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
                        <p style="text-align: left; font-size: .9em; margin-bottom: -1%;">Data Criação:</p>    
                        <p style="font-size: 1em; font-weight: bolder; color: rgb(168, 168, 168); text-align: left;">
                            <i class="pe-7s-stopwatch"></i> 
                            ${ element.data_criacao }
                        </p>
                    </div>
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xl-6">
                        <p style="text-align: right; font-size: .9em; margin-bottom: -1%;">Data Finalização:</p>
                        <p style="font-size: 1em; font-weight: bolder; color: rgb(168, 168, 168); text-align: right;">
                            <i class="pe-7s-timer"></i> 
                            ${ element.data_finalizacao }
                        </p>
                    </div>
                </div>
                <br/>          
                <br/>             
            </div>
        </div>
    `
}

function criarQuadro(){
    let id_profissional = JSON.parse(window.localStorage.getItem('idsCards')).id_profissional;
    let id_cliente = JSON.parse(window.localStorage.getItem('idsCards')).id_cliente;
    let id_solicitacao = JSON.parse(window.localStorage.getItem('idsCards')).id_solicitacao;
    fetch(api+'/api/projeto/criar', {
        method: 'POST',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        }),
        body:
        'titulo='+document.getElementById('titulo').value+
        '&descricao='+document.getElementById('descricao').value+
        '&data_criacao='+document.getElementById('data_criacao').value+
        '&data_finalizacao='+document.getElementById('data_finalizacao').value+
        '&id_user='+id_profissional+
        '&id_solicitacao='+id_solicitacao+
        '&id_cliente='+id_cliente
    })
    .then(sucesso => {
        sucesso.json()
        .then(Certo => {
            console.log('SUCESSO: ', Certo);
            window.location.href = 'acompanhamento.html';
        })
    })
    .catch(erro => {
        console.log('ERRO: ', erro);
    })
}

function deletar(id){
    console.log(id);
    fetch(api+'/api/projeto/deletar/'+id, {
        method: 'DELETE',
        headers: new Headers({
            'Accept': 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        })
    })
    .then(sucesso => {
        sucesso.json()
        .then(Certo => {
            window.location.href = 'acompanhamento.html';
        })
    })
    .catch(erro => {
        console.log('ERRO: ', erro);
    })
}