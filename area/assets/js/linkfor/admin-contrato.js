
var api = "https://www.linkfor.com.br/admin/public";
// var api = "http://127.0.0.1:8000";
var dados;
var usuarios = document.getElementById('admin-contrato');
document.getElementById('name-user').innerHTML = JSON.parse(window.localStorage.getItem('usuario')).name;
var page = document.getElementById('paginacao');

fetch(api+'/api/contrato/mostrar',{
    method: 'GET',
    headers: new Headers({
    'Accept': 'application/json',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Authorization': 'Bearer ' + window.localStorage.getItem('token')
    })
})
.then((profissionais)=>{
    profissionais.json()
    .then(certo => {
        dados = certo;
        for(let x = 0; x < certo.length; x++){
            var pag = document.createElement('li');
            pag.setAttribute("class", "page-item");
            pag.innerHTML = pagination(x);
            page.appendChild(pag);
        }

        certo.forEach(element => {
            var div = document.createElement('tr');
            div.innerHTML = projetos(element);
            usuarios.appendChild(div);
        });
    })
    .catch(erro => {
        console.log('DEU ERRADO:');
    })
})
.catch((err)=>{
    console.log(err)
})

function pagination (number){
    return `
        <a class="page-link active" onclick="prox(${ number+1 })" style="cursor: pointer;">
            ${ number+1 }
        </a>
    `
}


function prox(number){
    console.log('DADDA: ', number);
    fetch(dados.path+'?page=' + number,{
        method: 'GET',
        headers: new Headers({
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + window.localStorage.getItem('token')
        })
    })
    .then(cdadad => {
        cdadad.json()
        .then(certo => {
            usuarios.innerHTML = '';
            certo.data.forEach(element => {
                var div = document.createElement('tr');
                div.innerHTML = projetos(element);
                usuarios.appendChild(div);
            });
        })
    })
}

function projetos(element){
    let nomeProfissional;

    if(element.nome_profissional == null)
        nomeProfissional = '';
    else
        nomeProfissional = element.contrato.nome_profissional;

    return `
        <tr  onclick="visualizar(${ element.contrato.id })">
            <td >
                #${ element.contrato.id }
            </td>
            <td>
                ${ element.contrato.nome_profissional }
            </td>
            <td>
                ${ element.contrato.nome_cliente }
            </td>
            <td>
                ${ element.contrato.id_solicitacao }
            </td>
            <td>
                <a class="btn btn-success btn-fill btn-wd"  data-toggle="modal" data-target=".bd-example-modal-lg"  onclick="MostrarDados(${ element.contrato.id })">
                    <i class="fa fa-envelope"></i>
                    Visualizar
                </a>
                <a class="btn btn-danger btn-fill btn-wd" onclick="excluirMensagem()">
                    <i class="fa fa-trash"></i>
                    Excluir
                </a>
            </td>
        </tr>
    `;


}

function showNotification(from, align) {
    type = ['', 'info', 'danger','success', 'warning', 'rose', 'primary'];

    color = Math.floor((Math.random() * 6) + 1);

    $.notify({
        icon: "notifications",
        message: "Recurso ainda não disponível. Aguarde, em breve."

    }, {
        type: type[color],
        timer: 3000,
        placement: {
            from: from,
            align: align
        }
    });
}

function responderMensagem(id){
    alert('Recurso ainda não disponível');
}

function excluirMensagem(id){
    console.log(id);
}

function MostrarDados(element){
    dados.forEach(dados => {
        if(dados.contrato.id == element){
            document.getElementById('cpf_profissional_contrato').innerHTML = dados.contrato.cpf_profissional;
            document.getElementById('nome_profissional_contrato').innerHTML = dados.contrato.nome_profissional;
            document.getElementById('rg_profissional_contrato').innerHTML = dados.contrato.rg_profissional;
            document.getElementById('cnpj_profissional_contrato').innerHTML = dados.contrato.cnpj_profissional;
            document.getElementById('cnpj_cliente_contrato').innerHTML = dados.contrato.cnpj_cliente;
            document.getElementById('cpf_cliente_contrato').innerHTML = dados.contrato.cpf_cliente;
            document.getElementById('endereco_cep').innerHTML = dados.endereco_profissional[0].cep;
            document.getElementById('endereco_profissional-contrato').innerHTML = dados.endereco_profissional[0].endereco;
            document.getElementById('endereco_numero').innerHTML = dados.endereco_profissional[0].numero;
            document.getElementById('endereco_complemento').innerHTML = dados.endereco_profissional[0].bairro;

            document.getElementById('nome_cliente_contrato').innerHTML = dados.contrato.nome_cliente;
            document.getElementById('rg_cliente_contrato').innerHTML = dados.contrato.rg_cliente;
            document.getElementById('valor_projeto_contrato').innerHTML = dados.contrato.valor_projeto;
            document.getElementById('prazo_projeto').innerHTML = dados.contrato.prazo_execucao;
            document.getElementById('escopo_projeto').innerHTML = dados.contrato.escopo_projeto;
            document.getElementById('quantidade_parcelas_projeto').innerHTML = dados.contrato.quantidade_parcelas;
            
            document.getElementById('endereco_cep_c').innerHTML = dados.endereco_cliente[0].cep;
            document.getElementById('endereco_cliente-contrato').innerHTML = dados.endereco_cliente[0].endereco;
            document.getElementById('endereco_numero_c').innerHTML = dados.endereco_cliente[0].numero;
            document.getElementById('endereco_complemento_c').innerHTML = dados.endereco_cliente[0].bairro;
        }
    })
    // document.getElementById('cpf_profissional_contrato').innerHTML = dados.contrato.cpf_profissional;
    // document.getElementById('nome_profissional_contrato').innerHTML = dados.contrato.nome_profissional;
    // document.getElementById('rg_profissional_contrato').innerHTML = dados.contrato.rg_profissional;
    // document.getElementById('cnpj_profissional_contrato').innerHTML = dados.contrato.cnpj_profissional;
    // document.getElementById('cnpj_cliente_contrato').innerHTML = dados.contrato.cnpj_cliente;
    // document.getElementById('cpf_cliente_contrato').innerHTML = dados.contrato.cpf_cliente;
    // document.getElementById('endereco_cep').innerHTML = dados.endereco_profissional[0].cep;
    // document.getElementById('endereco_profissional-contrato').innerHTML = dados.endereco_profissional[0].endereco;
    // document.getElementById('endereco_numero').innerHTML = dados.endereco_profissional[0].numero;
    // document.getElementById('endereco_complemento').innerHTML = dados.endereco_profissional[0].bairro;

    // document.getElementById('nome_cliente_contrato').innerHTML = dados.contrato.nome_cliente;
    // document.getElementById('rg_cliente_contrato').innerHTML = dados.contrato.rg_cliente;
    // document.getElementById('valor_projeto_contrato').innerHTML = dados.contrato.valor_projeto;
    // document.getElementById('prazo_projeto').innerHTML = dados.contrato.prazo_execucao;
    // document.getElementById('escopo_projeto').innerHTML = dados.contrato.escopo_projeto;
    // document.getElementById('quantidade_parcelas_projeto').innerHTML = dados.contrato.quantidade_parcelas;
    
    // document.getElementById('endereco_cep_c').innerHTML = dados.endereco_cliente[0].cep;
    // document.getElementById('endereco_cliente-contrato').innerHTML = dados.endereco_cliente[0].endereco;
    // document.getElementById('endereco_numero_c').innerHTML = dados.endereco_cliente[0].numero;
    // document.getElementById('endereco_complemento_c').innerHTML = dados.endereco_cliente[0].bairro;
}
